import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicImageViewerModule } from 'ionic-img-viewer';

// plugin
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
import { OneSignal } from '@ionic-native/onesignal';
import { GooglePlus } from '@ionic-native/google-plus';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { Facebook } from '@ionic-native/facebook';
import { Geolocation } from '@ionic-native/geolocation';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { SocialSharing } from '@ionic-native/social-sharing';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { Camera } from '@ionic-native/camera';
import { CalendarModule } from "ion2-calendar";

// page
import { ConferenceApp } from './app.component';

// user
import { ContactPage } from '../pages/contact/contact';
import { AccountPage } from '../pages/account/account';
import { LoginPage } from '../pages/login/login';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { SettingPage } from '../pages/setting/setting';
import { EditProfilePage } from '../pages/edit-profile/edit-profile'
import { ChangePasswordPage } from '../pages/change-password/change-password'
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password'

// home
import { HomePage } from '../pages/home/home';
import { ArticleDetailPage } from '../pages/article-detail/article-detail';
import { NewsDetailPage } from '../pages/news-detail/news-detail';

// indonesia tax guide
import { TaxRatesPage } from '../pages/tax-rates/tax-rates';
import { TaxTreatyPage } from '../pages/tax-treaty/tax-treaty';
import { TaxTimeTestPage } from '../pages/tax-time-test/tax-time-test';
import { MapCodePage } from '../pages/map-code/map-code';
import { KursPajakPage } from '../pages/kurs-pajak/kurs-pajak';
import { IndonesiaTaxGuideDetailPage } from '../pages/indonesia-tax-guide-detail/indonesia-tax-guide-detail'

// learning tax
import { InfografikPage } from '../pages/infografik/infografik';
import { VideoPage } from '../pages/video/video';
import { LearningTaxDetailPage } from '../pages/learning-tax-detail/learning-tax-detail'

// tax office location
import { TaxOfficeLocationPage } from '../pages/tax-office-location/tax-office-location';

// custom seminar (soon build)
import { CustomSeminarPage } from '../pages/custom-seminar/custom-seminar';
import { CustomSeminarModal } from '../pages/custom-seminar/custom-seminar';
import { CustomSeminarRegisterPage } from '../pages/custom-seminar-register/custom-seminar-register';
import { CustomSeminarDetailPage } from '../pages/custom-seminar-detail/custom-seminar-detail';

// taxation book (soon build)
import { TaxationBookPage } from '../pages/taxation-book/taxation-book';
import { TaxationBookDetailPage } from '../pages/taxation-book-detail/taxation-book-detail';

// tax and custom regulation (soon build)
import { TaxAndCustomRegulationPage } from '../pages/tax-and-custom-regulation/tax-and-custom-regulation';
import { TaxAndCustomRegulationDetailPage } from '../pages/tax-and-custom-regulation-detail/tax-and-custom-regulation-detail';

// data
import { ConferenceData } from '../providers/conference-data';
import { UserData } from '../providers/user-data';
import { ArticleData } from '../providers/article-data';
import { NewsData } from '../providers/news-data';
import { ContactUsData } from '../providers/contact-us-data';
import { SlideData } from '../providers/slider-data'
import { TaxRatesData } from '../providers/tax-rates-data'
import { TaxTreatyData } from '../providers/tax-treaty-data'
import { LoginRegisterData } from '../providers/login-register-data'
import { TaxTimeTestData } from '../providers/tax-time-test-data'
import { MapCodeData } from '../providers/map-code-data'
import { KursPajakData } from '../providers/kurs-pajak-data'
import { VideoData } from '../providers/video-data'
import { InfografikData } from '../providers/infografik-data'
import { TaxBooksData } from '../providers/tax-books-data'
import { SeminarData } from '../providers/seminar-data'
import { DataService } from '../data.service';
import { TaxCustomRegulationData } from '../providers/tax-custom-regulation-data';

@NgModule({
  declarations: [
    ConferenceApp,
    SettingPage,
    ContactPage,
    AccountPage,
    LoginPage,
    TutorialPage,
    EditProfilePage,
    ChangePasswordPage,
    ForgotPasswordPage,

    HomePage,
    NewsDetailPage,
    ArticleDetailPage,

    TaxRatesPage,
    TaxTreatyPage,
    TaxTimeTestPage,
    MapCodePage,
    KursPajakPage,
    IndonesiaTaxGuideDetailPage,

    InfografikPage,
    VideoPage,
    LearningTaxDetailPage,

    TaxOfficeLocationPage,

    CustomSeminarPage,
    CustomSeminarModal,
    CustomSeminarDetailPage,
    CustomSeminarRegisterPage,

    TaxationBookPage,
    TaxationBookDetailPage,

    TaxAndCustomRegulationPage,
    TaxAndCustomRegulationDetailPage
      ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    CalendarModule,
    IonicModule.forRoot(ConferenceApp, {}, {
      links: [
        { component: ContactPage, name: 'About', segment: 'about' },
        { component: TutorialPage, name: 'Tutorial', segment: 'tutorial' },
        { component: LoginPage, name: 'LoginPage', segment: 'login' },
        { component: AccountPage, name: 'AccountPage', segment: 'account' },
        { component: SettingPage, name: 'SettingPage', segment: 'setting' },
        { component: EditProfilePage, name: 'EditProfilePage', segment: 'EditProfile' },
        { component: ChangePasswordPage, name: 'ChangePasswordPage', segment: 'ChangePassword' },
        { component: ForgotPasswordPage, name: 'ForgotPasswordPage', segment: 'ForgotPassword' },

        { component: HomePage, name: 'HomePage', segment: 'home' },
        { component: NewsDetailPage, name: 'newsDetail', segment: 'newsDetail/:sessionId' },
        { component: ArticleDetailPage, name: 'contentDetail', segment: 'contentDetail/:sessionId' },

        { component: TaxRatesPage, name: 'TaxRatesPage', segment: 'TaxRates' },
        { component: TaxTreatyPage, name: 'TaxTreatyPage', segment: 'TaxTreaty' },
        { component: TaxTimeTestPage, name: 'TaxTimeTestPage', segment: 'TaxTimeTest' },
        { component: MapCodePage, name: 'MapCodePage', segment: 'MapCode' },
        { component: KursPajakPage, name: 'KursPajakPage', segment: 'KursPajak' },
        { component: IndonesiaTaxGuideDetailPage, name: 'IndonesiaTaxGuideDetailPage', segment: 'IndonesiaTaxGuideDetail' },

        { component: VideoPage, name: 'VideoPage', segment: 'video' },
        { component: InfografikPage, name: 'InfografikPage', segment: 'Infografik' },
        { component: LearningTaxDetailPage, name: 'LearningTaxDetailPage', segment: 'LearningTaxDetail' },

        { component: TaxOfficeLocationPage, name: 'TaxOfficeLocationPage', segment: 'taxOfficeLocation' },

        { component: CustomSeminarPage, name: 'CustomSeminarPage', segment: 'CustomSeminar' },
        { component: CustomSeminarDetailPage, name: 'CustomSeminarDetailPage', segment: 'CustomSeminarDetail' },
        { component: CustomSeminarRegisterPage, name: 'CustomSeminarRegisterPage', segment: 'CustomSeminarRegister' },

        { component: TaxationBookPage, name: 'TaxationBookPage', segment: 'TaxationBook' },
        { component: TaxAndCustomRegulationPage, name: 'TaxAndCustomRegulationPage', segment: 'TaxAndCustomRegulation' },
        { component: TaxAndCustomRegulationDetailPage, name: 'TaxAndCustomRegulationDetailPage', segment: 'TaxAndCustomRegulationDetail' },

      ]
    }),
    IonicStorageModule.forRoot(),
    SuperTabsModule.forRoot(),
    IonicImageViewerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    ConferenceApp,
    SettingPage,
    ContactPage,
    AccountPage,
    LoginPage,
    TutorialPage,
    EditProfilePage,
    ChangePasswordPage,
    ForgotPasswordPage,

    HomePage,
    NewsDetailPage,
    ArticleDetailPage,

    TaxRatesPage,
    TaxTreatyPage,
    TaxTimeTestPage,
    MapCodePage,
    KursPajakPage,
    IndonesiaTaxGuideDetailPage,

    InfografikPage,
    VideoPage,
    LearningTaxDetailPage,

    TaxOfficeLocationPage,

    CustomSeminarPage,
    CustomSeminarModal,
    CustomSeminarDetailPage,
    CustomSeminarRegisterPage,

    TaxationBookPage,
    TaxationBookDetailPage,

    TaxAndCustomRegulationPage,
    TaxAndCustomRegulationDetailPage,
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ConferenceData,

    ContactUsData,
    UserData,
    ArticleData,
    LoginRegisterData,
    NewsData,
    TaxRatesData,
    TaxTreatyData,
    TaxTimeTestData,
    KursPajakData,
    MapCodeData,
    VideoData,
    InfografikData,
    TaxBooksData,
    SeminarData,
    SlideData,
    TaxCustomRegulationData,
    InAppBrowser,
    SplashScreen,
    OneSignal,
    GooglePlus,
    UniqueDeviceID,
    DataService,
    Geolocation,
    LaunchNavigator,
    SocialSharing,
    Facebook,
    Camera,
    DatePipe
  ]
})
export class AppModule { }

