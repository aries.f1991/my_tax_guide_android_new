import { Component, ViewChild } from '@angular/core';
import { Events, MenuController, Nav, Platform, ToastController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { ContactPage } from '../pages/contact/contact';
// import { LoginPage } from '../pages/login/login';
import { TaxRatesPage } from '../pages/tax-rates/tax-rates';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { TaxOfficeLocationPage } from '../pages/tax-office-location/tax-office-location';
import { HomePage } from '../pages/home/home';
import { ConferenceData } from '../providers/conference-data';
import { UserData } from '../providers/user-data';
import { TaxTreatyPage} from "../pages/tax-treaty/tax-treaty";
import { TaxTimeTestPage} from "../pages/tax-time-test/tax-time-test";
import { MapCodePage} from "../pages/map-code/map-code";
import { KursPajakPage} from "../pages/kurs-pajak/kurs-pajak";
import { InfografikPage} from "../pages/infografik/infografik";
import { VideoPage} from "../pages/video/video";
import { CustomSeminarPage} from "../pages/custom-seminar/custom-seminar";
import { TaxationBookPage} from "../pages/taxation-book/taxation-book";
import { TaxAndCustomRegulationPage} from "../pages/tax-and-custom-regulation/tax-and-custom-regulation";


export interface PageInterface {
  title: string;
  name: string;
  component: any;
  icon: number;
  logsOut?: boolean;
  index?: number;
  tabName?: string;
  tabComponent?: any;
}

@Component({
  templateUrl: 'app.template.html'
})
export class ConferenceApp {
  // the root nav is a child of the root app component
  // @ViewChild(Nav) gets a reference to the app's root nav
  @ViewChild(Nav) nav: Nav;
  counter = 0

  // List of pages that can be navigated to from the left menu
  // the left menu only works after login
  // the login page disables the left menu
  appPages: PageInterface[] = [
    { title: 'Beranda',                      name: '', component: HomePage, icon: 0 },
	{ title: 'Pencarian Peraturan Perpajakan',  name: '', component: TaxAndCustomRegulationPage, icon: 0 },
    { title: 'Panduan Perpajakan Indonesia',       name: '', component: TaxRatesPage, icon: 11 },
    { title: 'Tarif Pajak',                 name: '', component: TaxRatesPage, icon: 1 },
    { title: 'P3B',                name: '', component: TaxTreatyPage, icon: 1 },
    { title: 'Time Test',             name: '', component: TaxTimeTestPage, icon: 1 },
    { title: 'Kode Map',                  name: '', component: MapCodePage, icon: 1 },
    { title: 'Kurs Pajak',                name: '', component: KursPajakPage, icon: 1 },
    { title: 'Belajar Perpajakan',              name: '', component: InfografikPage, icon: 12 },
    { title: 'Infografis',                name: '', component: InfografikPage, icon: 2 },
    { title: 'Video',                     name: '', component: VideoPage, icon: 2 },
    { title: 'Jadwal Seminar Pajak',  name: '', component: CustomSeminarPage, icon: 0 },
    { title: 'Buku Perpajakan',             name: '', component: TaxationBookPage, icon: 0 },
    { title: 'Lokasi Kantor Pajak',       name: '', component: TaxOfficeLocationPage, icon: 0 },
    { title: 'Hubungi Kami',                name: '', component: ContactPage, icon: 0 },
    // { title: 'Log In',                    name: '', component: LoginPage, icon: 0 }
  ];

  rootPage: any;

  public childMenu1 = false;
  public childMenu2 = false;

  constructor(
    public events: Events,
    public userData: UserData,
    public menu: MenuController,
    public platform: Platform,
    public confData: ConferenceData,
    public splashScreen: SplashScreen,
    public storage:Storage,
    public toastController:ToastController,
    public plt: Platform
  ) {

    // load the conference data
    confData.load();

    // decide which menu items should be hidden by current login status stored in local storage
    this.userData.hasLoggedIn().then((hasLoggedIn) => {
      this.enableMenu(hasLoggedIn === true);
    });
    this.enableMenu(true);

    this.listenToLoginEvents();
    // Check if the user has already seen the tutorial
    this.storage.get('hasSeenTutorial')
      .then((hasSeenTutorial) => {
        if (hasSeenTutorial) {
          this.rootPage = HomePage;
        } else {
          this.rootPage = TutorialPage;
        }
        this.platformReady()
      });
  }

  ngOnInit(){

    this.plt.ready().then((readySource) => {
      console.log('Platform ready ', readySource);
      this.userData.valueToken()
      localStorage.setItem('text_size', '14')
    });
  }

  openPage(page: PageInterface) {
    if (page.icon == 11 || page.icon == 12) {
      if (page.icon == 11) {
        this.childMenu1 = !this.childMenu1
      }
      if (page.icon == 12) {
        this.childMenu2 = !this.childMenu2
      }
    }else{
      this.nav.setRoot(page.component);
      this.childMenu1 = false
      this.childMenu2 = false
    }
  }

  openTutorial() {
    this.nav.setRoot(TutorialPage);
  }

  listenToLoginEvents() {
    this.events.subscribe('user:login', () => {
      this.enableMenu(true);
    });

    this.events.subscribe('user:signup', () => {
      this.enableMenu(true);
    });

    this.events.subscribe('user:logout', () => {
      this.enableMenu(false);
    });
  }

  enableMenu(loggedIn: boolean) {
    this.menu.enable(loggedIn, 'loggedInMenu');
    this.menu.enable(!loggedIn, 'loggedOutMenu');
  }

  platformReady() {
    // Call any initial plugins when ready
    this.platform.ready().then(() => {
      this.splashScreen.hide();

      this.platform.registerBackButtonAction(() => {
        if (this.counter == 0) {
          this.counter++;
          this.presentToast();
          setTimeout(() => { this.counter = 0 }, 3000)
        } else {
          // console.log("exitapp");
          this.platform.exitApp();
        }
      }, 0)
    });
  }

  presentToast() {
    let toast = this.toastController.create({
      message: "Tekan sekali lagi untuk keluar",
      duration: 1000,
	  cssClass: "toastExit",
	  position: 'top'
    });
    toast.present();
  }

  isActive(page: PageInterface) {
    let childNav = this.nav.getActiveChildNavs()[0];

    // Tabs are a special case because they have their own navigation
    if (childNav) {
      if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
        return 'primary';
      }
      return;
    }

    if (this.nav.getActive() && this.nav.getActive().name === page.name) {
      return 'primary';
    }
    return;
  }
}
