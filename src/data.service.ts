import { Injectable } from '@angular/core'
import { Storage } from '@ionic/storage';
import { Headers } from '@angular/http'

@Injectable()
export class DataService {

  public headers
  public APIKeyGoogle     = 'AIzaSyB_z2DgWWo8o6AugQAmn6fjX6ymj9-3j3o'

  // ---- URL for RESTful API -----

  public baseUrl = 'https://api.mytaxguide.id/'

  public url      = []
  public detail   = '-detail'
  public image    = '-image'
  public search   = '-search'
  public comment  = '-comment'
  public related  = '-related'
  public distance = '-distance'
  public login    = '-login'
  public social   = '-login_social_media'
  public register = '-registration'
  public month    = '-month'
  public day      = '-day'
  public daySeminar     = '-daySeminar'
  public forgotPassword = '-forgot_password'
  public getDataProfile = '-get_data_profile'
  public changePassword = '-change_password'
  public editProfile = '-edit_profile'
  public updateProfilePicture = '-update_profile_picture'

  // Indonesian Tax Guide Const
  public indonesianTaxGuideCategory       = ['tax-rates', 'tax-treaty', 'tax-time-test', 'map-code', 'kurs-pajak']
  public indonesianTaxGuideCategoryConst  = [1, 2, 3, 4, 5]

  // Indonesian Learning Tax Const
  public indonesianLearningTaxCategory      = ['infografik', 'video']
  public indonesianLearningTaxCategoryConst = [1, 2]
  
  public origins = '&origins='
  public destinations = '&destinations='

  private countLimit = 10
  private countLimitRelated = 5
  
  public withLimiterAndPage = '&limit='+ this.countLimit +'&page='
  public withLimiterAndPageRelated = '&limit='+ this.countLimitRelated

  // constructor
  constructor(
    public storage: Storage
  ) {
    this.setURL()
    this.setHeader()
  }

  setHeader(){
    this.headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'token' : localStorage.getItem('token'),
      'token-login': localStorage.getItem('login_token')
    });    
    return this.headers
  }

  private setURL(){
    // get Token
    this.url['token']                   = this.baseUrl + "token/create_token"

    // tax office location
    this.url['tax-office-location']     = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=kantor-pajak&radius=10000&key=" + this.APIKeyGoogle + "&location="

    this.url['tax-office-location' + this.distance] = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&key=" + this.APIKeyGoogle

    // contact us
    this.url['contact-us']              = this.baseUrl + 'contact_us/post_contact_us'

    // comment
    this.url['comment']                 = this.baseUrl + 'comment/input_comment'

    // login and register
    this.url['login-register' + this.login]           = this.baseUrl + 'member/login'
    this.url['login-register' + this.social]          = this.baseUrl + 'member/login_social_media'
    this.url['login-register' + this.register]        = this.baseUrl + 'member/registration'
    this.url['login-register' + this.getDataProfile]  = this.baseUrl + 'member/get_data_profile'
    this.url['login-register' + this.forgotPassword]  = this.baseUrl + 'member/reset_password'
    this.url['login-register' + this.changePassword]  = this.baseUrl + 'member/change_password'
    this.url['login-register' + this.editProfile]     = this.baseUrl + 'member/edit_profile'
    this.url['login-register' + this.updateProfilePicture] = this.baseUrl + 'member/update_profile_picture'

    // slide
    this.url['slide']                   = this.baseUrl + 'article/get_slider'

    // news
    this.url['news']                    = this.baseUrl + 'article/get_list_news?limit=' + this.countLimit + '&page='
    this.url['news' + this.search]      = this.baseUrl + 'article/get_search_result_news?search='

    this.url['news' + this.detail]      = this.baseUrl + 'article/get_detail_news?item_id='
    this.url['news' + this.image]       = this.baseUrl + 'article/get_detail_news_images?item_id='
    this.url['news' + this.related]     = this.baseUrl + 'article/get_related_news?item_id='
    this.url['news' + this.comment]     = this.baseUrl + 'article/get_comment_news?item_id='

    // article
    this.url['article']                 = this.baseUrl + 'article/get_list_article?limit=' + this.countLimit + '&page='
    this.url['article' + this.search]   = this.baseUrl + 'article/get_search_result_article?search='

    this.url['article' + this.detail]   = this.baseUrl + 'article/get_detail_article?item_id='
    this.url['article' + this.image]    = this.baseUrl + 'article/get_detail_article_images?item_id='
    this.url['article' + this.related]  = this.baseUrl + 'article/get_related_article?item_id='
    this.url['article' + this.comment]  = this.baseUrl + 'article/get_comment_article?item_id='

    // seminar
    this.url['seminar']                 = this.baseUrl + 'seminar/get_list_seminar?limit=' + 370 + '&page='
    this.url['seminar' + this.search]   = this.baseUrl + 'seminar/get_search_result_seminar?search='

    this.url['seminar' + this.detail]     = this.baseUrl + 'seminar/get_detail_seminar?item_id='
    this.url['seminar' + this.image]      = this.baseUrl + 'seminar/get_detail_seminar_images?item_id='
    this.url['seminar' + this.related]    = this.baseUrl + 'seminar/get_related_seminar?item_id='
    this.url['seminar' + this.comment]    = this.baseUrl + 'seminar/get_comment_seminar?item_id='
    this.url['seminar' + this.month]      = this.baseUrl + 'seminar/get_list_seminar_bymonth'
    this.url['seminar' + this.day]        = this.baseUrl + 'seminar/get_list_seminar_bydate'
    this.url['seminar' + this.daySeminar] = this.baseUrl + 'seminar/get_list_seminar_bydateseminar'
    this.url['seminar' + this.register]   = this.baseUrl + 'seminar/input_pendaftaran_seminar'

    // tax books
    this.url['tax-books']                 = this.baseUrl + 'tax_books/get_list_tax_books?limit=' + this.countLimit + '&page='
    this.url['tax-books' + this.search]   = this.baseUrl + 'tax_books/get_search_result_tax_books?search='

    this.url['tax-books' + this.detail]   = this.baseUrl + 'tax_books/get_detail_tax_books?item_id='
    this.url['tax-books' + this.image]    = this.baseUrl + 'tax_books/get_detail_tax_books_images?item_id='
    this.url['tax-books' + this.related]  = this.baseUrl + 'tax_books/get_related_tax_books?item_id='
    this.url['tax-books' + this.comment]  = this.baseUrl + 'tax_books/get_comment_tax_books?item_id='
    
    //tax custom regulation
    this.url['kurs-pajak-level']               = this.baseUrl + 'tax_custom_regulation/get_list_level'
    this.url['kurs-pajak-topik']               = this.baseUrl + 'tax_custom_regulation/get_list_topik'
    this.url['kurs-pajak-searchTaxRegulation'] = this.baseUrl + 'tax_custom_regulation/get_search_result_tax_custom_regulation?'
    this.url['kurs-pajak-detailTaxRegulation'] = this.baseUrl + 'tax_custom_regulation/get_detail_tax_custom_regulation?item_id='

    // Indonesian Tax Guide Category
    for (let i = 0; i < this.indonesianTaxGuideCategory.length; i++) {
      this.url[this.indonesianTaxGuideCategory[i]]                 = this.baseUrl + 'indonesia_tax_guide/get_list_indonesia_tax_guide?limit=' + this.countLimit + '&category_id='+ this.indonesianTaxGuideCategoryConst[i] +'&page='
      this.url[this.indonesianTaxGuideCategory[i] + this.search]   = this.baseUrl + 'indonesia_tax_guide/get_search_result_indonesia_tax_guide?search='
  
      this.url[this.indonesianTaxGuideCategory[i] + this.detail]   = this.baseUrl + 'indonesia_tax_guide/get_detail_indonesia_tax_guide?item_id='
      this.url[this.indonesianTaxGuideCategory[i] + this.image]    = this.baseUrl + 'indonesia_tax_guide/get_detail_indonesia_tax_guide_images?item_id='
      this.url[this.indonesianTaxGuideCategory[i] + this.related]  = this.baseUrl + 'indonesia_tax_guide/get_related_indonesia_tax_guide?item_id='
      this.url[this.indonesianTaxGuideCategory[i] + this.comment]  = this.baseUrl + 'indonesia_tax_guide/get_comment_indonesia_tax_guide?item_id='
    }

    // Indonesian Learning Tax
    for (let i = 0; i < this.indonesianLearningTaxCategory.length; i++) {
      this.url[this.indonesianLearningTaxCategory[i]]                 = this.baseUrl + 'learning_tax/get_list_learning_tax?limit=' + this.countLimit + '&category_id='+ this.indonesianLearningTaxCategoryConst[i] +'&page='
      this.url[this.indonesianLearningTaxCategory[i] + this.search]   = this.baseUrl + 'learning_tax/get_search_result_learning_tax?search='
  
      this.url[this.indonesianLearningTaxCategory[i] + this.detail]   = this.baseUrl + 'learning_tax/get_detail_learning_tax?item_id='
      this.url[this.indonesianLearningTaxCategory[i] + this.image]    = this.baseUrl + 'learning_tax/get_detail_learning_tax_images?item_id='
      this.url[this.indonesianLearningTaxCategory[i] + this.related]  = this.baseUrl + 'learning_tax/get_related_learning_tax?item_id='
      this.url[this.indonesianLearningTaxCategory[i] + this.comment]  = this.baseUrl + 'learning_tax/get_comment_learning_tax?item_id='
    }

    console.log(this.url)

  }

  arrayToQueryString(array_in){
    let out = new Array();
    for(let key in array_in){
        out.push(key + '=' + encodeURIComponent(array_in[key]));
    }
    return out.join('&');
  }
}
