import { Injectable } from '@angular/core'
import { BaseData } from './base-data'

@Injectable()
export class InfografikData extends BaseData{
	nameClass = 'infografik';
}