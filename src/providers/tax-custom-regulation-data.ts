import { Injectable } from '@angular/core'
import { BaseData } from './base-data'
import 'rxjs/add/operator/map'
import 'rxjs/add/observable/of'
import 'rxjs/add/operator/timeout'

@Injectable()
export class TaxCustomRegulationData extends BaseData{
	public nameClass = 'kurs-pajak';

  getDetail(id){
    return this.loadDetailItem(id).map((data: any) => {
      return data
    })
  }

	getItemsSearchTaxRegulation(items, page) {
	return this.loadDataSearch(items, page).map((data: any) => {
	  if (data === null){
	    data = {
        "responseStatus": 200,
        "total_tax_custom_regulation": 0,
        "page": "1",
        "data": []
      }
	  }
	  return data
    })
  }

  // function for load data item from server
  private loadDetailItem(id): any {
    return this.http.get(this.dS.url[this.nameClass] + id,
      {headers:this.dS.setHeader()})
      .map(res => res.json())
  }

  private loadDataSearch(items, page) {

    console.log(items)

    let final = ""

    if(items.keyword != undefined)
      final += '&text=' + items.keyword

    if(items.ruleNumber != undefined)
      final += '&no_aturan=' + items.ruleNumber

    if(items.tahunPajak != undefined)
      final += '&tahun_pajak=' + items.tahunPajak

    if(items.level != undefined && items.level != '-- Empty --')
      final += '&level=' + items.level

    if(items.topik != undefined && items.topik != '-- Empty --')
      final += '&topik=' + items.topik

    console.log(final)

    return this.http.get(this.dS.url[this.nameClass] + "limit=10" + final + "&page=" + page,
      {headers:this.dS.setHeader()})
      .map(res => res.json())
  }
}
