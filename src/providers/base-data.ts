import { Injectable } from '@angular/core'
import { Http } from '@angular/http'
import { Storage } from '@ionic/storage';

import 'rxjs/add/operator/map'
import 'rxjs/add/observable/of'
import 'rxjs/add/operator/timeout'

import { DataService } from '../data.service'
// providers for base data
// - interface data to system -
//
// function :
// public

// - getEmpty()       -> get data empty by default, return (data default)
// - getItems() -> get all data from server API, return (data mapping)
// - getItem()        -> get selection data from local with ID, return (data mapping selection)
// - setItem          -> interface function for switch postItem() & putItem()
// - deleteItem()     -> delete data to server with ID, return (server response)

// private
// - initData()       -> make data set default, void
// - loadServer()     -> get data from localstorage, return (data from server)
// - processData()    -> mapping data & save to localstorage, return (data mapping)
// - postItem()       -> post data to server with CREDS data, return (server response)
// - putItem()        -> put/edit data to server with CREDS data & ID, return (server response)

@Injectable()
export class BaseData {

  public nameClass

  constructor(
    public storage: Storage,
    public dS: DataService,
    public http: Http)
  {
  }

  // list data
  // function for get data server
  getItems(page, addition = "", param = "") {
    return this.loadServerItems(page, addition, param).map((data: any) => {
      return data
    })
  }

  // function for get data search server
  getItemsSearch(search) {
    return this.loadServerItemsSearch(search).map((data: any) => {
      return data
    })
  }
  // end list data

  // detail data 
  // function for get item data server
  getItem(id: any) {
    return this.loadServerItem(id).map((data: any) => {
      return data
    })
  }
  
  // function for get item image data server
  getItemImage(id: any) {
    return this.loadServerItemImage(id).map((data: any) => {
      return data
    })
  }

  // function for get item comment data server
  getItemComment(id: any, page: any) {
    return this.loadServerItemComment(id, page).map((data: any) => {
      return data
    })
  }

  // function for get item related data server
  getItemRelated(id: any) {
    return this.loadServerItemRelated(id).map((data: any) => {
      return data
    })
  }
  // end detail data

  // function for post/update items 0 for insert & 1 for update
  setItem(id:any, creds:any, type:boolean, addition = ""){
    if (type) {
      return this.putItem(creds, id)
    }else{
      if(addition === "comment"){
        return this.postComment(creds)
      }
      return this.postItem(creds, addition)
    }
  }

  // function for delete item data
  deleteItem(id: any){
    return this.http.delete(
      this.dS.url[this.nameClass + this.dS.detail] + id,
      {headers:this.dS.setHeader()}
    )
      .map(res => res.json())
  }

  // function for post item data
  private postItem(creds: any, addition = ""){
    let data = this.dS.arrayToQueryString(creds)
    return this.http.post(
      this.dS.url[this.nameClass + addition],
      data,
      {headers:this.dS.setHeader()}
    )
      .map(res => res.json())
  }

  // function for post item data
  private postComment(creds: any){
    let data = this.dS.arrayToQueryString(creds)
    return this.http.post(
      this.dS.url["comment"],
      data,
      {headers:this.dS.setHeader()}
    )
      .map(res => res.json())
  }

  // interface function for put & edit item data
  private putItem(creds: any, id: any){
    let data = this.dS.arrayToQueryString(creds)
    return this.http.put(
      this.dS.url[this.nameClass] + id,
      data,
      {headers:this.dS.setHeader()}
    )
      .map(res => res.json())
  }

  // function for load data items from server
  private loadServerItems(page, addition = "", param = ""): any {
    return this.http.get(this.dS.url[this.nameClass + addition] + page + param,
      {headers:this.dS.setHeader()})
      .map(res => res.json())
  }

  // function for load data items from server
  private loadServerItemsSearch(search): any {
    return this.http.get(this.dS.url[this.nameClass + this.dS.search] + search,
      {headers:this.dS.setHeader()})
      .map(res => res.json())
  }

  // function for load data item from server
  private loadServerItem(id): any {
    return this.http.get(this.dS.url[this.nameClass + this.dS.detail] + id,
      {headers:this.dS.setHeader()})
      .map(res => res.json())
  }

  // function for load data item from server
  private loadServerItemImage(id): any {
    return this.http.get(this.dS.url[this.nameClass + this.dS.image] + id,
      {headers:this.dS.setHeader()})
      .map(res => res.json())
  }

  // function for load data item from server
  private loadServerItemComment(id, page): any {
    return this.http.get(this.dS.url[this.nameClass + this.dS.comment] + id + this.dS.withLimiterAndPage + page,
      {headers:this.dS.setHeader()})
      .map(res => res.json())
  }
  
  // function for load data item from server
  private loadServerItemRelated(id): any {
    return this.http.get(this.dS.url[this.nameClass + this.dS.related] + id + this.dS.withLimiterAndPageRelated,
      {headers:this.dS.setHeader()})
      .map(res => res.json())
  }
}
