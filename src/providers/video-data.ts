import { Injectable } from '@angular/core'
import { BaseData } from './base-data'

@Injectable()
export class VideoData extends BaseData{
	nameClass = 'video';
}