import { Injectable } from '@angular/core'
import { BaseData } from './base-data'

@Injectable()
export class TaxRatesData extends BaseData{
	nameClass = 'tax-rates';
}