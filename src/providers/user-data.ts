import { Injectable } from '@angular/core';

import { Events, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http'
import { DataService } from '../data.service'
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { OneSignal } from '@ionic-native/onesignal';
import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

@Injectable()
export class UserData {
  _favorites: string[] = [];
  HAS_LOGGED_IN = 'hasLoggedIn';
  HAS_SEEN_TUTORIAL = 'hasSeenTutorial';
  DEVICE_ID = 'deviceID';

  constructor(
    public events: Events,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    public http: Http,
    public dS: DataService,
    public uniqueDeviceID: UniqueDeviceID,
    private _notification: OneSignal,
    public alertCtrl: AlertController,
    public plt: Platform
  ) {

  }

  hasFavorite(sessionName: string): boolean {
    return (this._favorites.indexOf(sessionName) > -1);
  };

  addFavorite(sessionName: string): void {
    this._favorites.push(sessionName);
  };

  removeFavorite(sessionName: string): void {
    let index = this._favorites.indexOf(sessionName);
    if (index > -1) {
      this._favorites.splice(index, 1);
    }
  };

  login(username: string): void {
    this.storage.set(this.HAS_LOGGED_IN, true);
    this.setUsername(username);
    this.events.publish('user:login');
  };

  signup(username: string): void {
    this.storage.set(this.HAS_LOGGED_IN, true);
    this.setUsername(username);
    this.events.publish('user:signup');
  };

  logout(): void {
    this.storage.remove(this.HAS_LOGGED_IN);
    this.storage.remove('username');
    this.events.publish('user:logout');
  };

  getUsername()
  {
    return this.storage.get('name');
  }

  setUsername(username: string)
  {
    this.storage.set('username', username);
  }

  setName(name: string): void {
    this.storage.set('name', name);
  };

  getName(){
    return this.storage.get('name').then((value) => {
      return (value);
    });
  }

  setUrlPict(url: string): void{
    this.storage.set('urlPict', url);
  }

  getUrlPict()
  {
    return this.storage.get('urlPict');
  }

  setEmail(email: string)
  {
    this.storage.set('email', email);
  }

  getEmail()
  {
    return this.storage.get('email');
  }

  hasLoggedIn(): Promise<boolean> {
    return this.storage.get(this.HAS_LOGGED_IN).then((value) => {
      return value === true;
    });
  };

  checkHasSeenTutorial(): Promise<string> {
    return this.storage.get(this.HAS_SEEN_TUTORIAL).then((value) => {
      return value;
    });
  };

  setToken(){
    let data = []
    data['device_id'] = this.storage.get('user_data').then((value) =>{
      return value.deviceID;
    });

    const creds = JSON.stringify(data)

    this.http.post(this.dS.url['token'], creds).map(res => res.json())
    .subscribe(data => {
      this.storage.get('user_data').then((value) =>{
        let nilai = value;
        nilai.token = data.token;
        this.storage.set('user_data', nilai);
      });
      return true
    },
    err => {
      console.log(err)
      return false
    })
  };

  getToken(){
    return this.storage.get('user_data').then((value) => {
      return value.token;
    });
  }

  setDeviceID()
  {
    this.storage.get('user_data').then((value) => {
      let data = value;
      this.uniqueDeviceID.get()
        .then((uuid:any) => {
          data.deviceID = uuid;
          this.storage.set('user_data', data);
        });
    });
  }

  presentAlert(msg){
    let alert = this.alertCtrl.create({
      title: 'Info',
      message: msg,
      buttons: [
      {
        text: 'Ok',
        handler: () => {
          console.log('Button clicked');
        }
      }
      ]
    });
    alert.present();
  }

  valueToken(){
    let loading = this.loadingCtrl.create({
    content: 'Please wait...'
    });
    loading.present();
    this.uniqueDeviceID.get()
    .then((uuid:any) => {
      console.log("device_id " + uuid)
      localStorage.setItem('device_id', uuid)

      this._notification.startInit("58b914b9-87f1-49b2-be21-f7538a0ffa7e", "28361620506");
      this._notification.getIds()
        .then((ids) =>{
          console.log(ids)
          console.log(this.dS.url['token'])
          
          this.http.post(this.dS.url['token'],
                        'device_id=' + uuid + '&onesignal_id=' + ids.userId, 
                        {headers:this.dS.setHeader()})
            .map(res => res.json())
            .subscribe(data => {
            console.log("token: " + data.token)
            localStorage.setItem('token', data.token)
            loading.dismiss()
          },
          err => {
            this.presentAlert(err)
          }) 
        });

      this._notification.inFocusDisplaying(this._notification.OSInFocusDisplayOption.Notification);
      this._notification.setSubscription(true);
      this._notification.endInit();
    },
    err =>{
      this.presentAlert(err)
    })
    .catch((error: any) => 
    console.log("error: " + error));
  }

  getDeviceID()
  {
    return this.storage.get('user_data').then((value)=>{
      return value.deviceID;
    });
  }
  
}
