import { Injectable } from '@angular/core'
import { BaseData } from './base-data'

@Injectable()
export class TaxTreatyData extends BaseData{
	nameClass = 'tax-treaty';
}