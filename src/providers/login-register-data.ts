import { Injectable } from '@angular/core'
import { BaseData } from './base-data'
import 'rxjs/add/operator/map'
import 'rxjs/add/observable/of'
import 'rxjs/add/operator/timeout'

@Injectable()
export class LoginRegisterData extends BaseData {

  public nameClass = 'login-register'

  getItems(addition) {
    return this.loadServerItemsGet(addition).map((data: any) => {
      return data
    })
  }

   // function for load data items from server
  private loadServerItemsGet(addition): any {
    return this.http.get(this.dS.url[this.nameClass + addition],
      {headers:this.dS.setHeader()})
      .map(res => res.json())
  }
}