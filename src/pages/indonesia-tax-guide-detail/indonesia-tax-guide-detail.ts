import { Component } from '@angular/core'
import { NavController, NavParams, ToastController } from 'ionic-angular'
import { SocialSharing } from '@ionic-native/social-sharing'
import { SettingPage } from "../setting/setting"
import { AlertController } from 'ionic-angular';
import { TaxRatesData } from '../../providers/tax-rates-data';
import { DomSanitizer } from '@angular/platform-browser';
import { HomePage } from '../home/home'

@Component({
  selector: 'page-indonesia-tax-guide-detail',
  templateUrl: 'indonesia-tax-guide-detail.html'
})

export class IndonesiaTaxGuideDetailPage {
  fontSize: any = localStorage.getItem('text_size') + 'px'
  id: any
  session: any
  detailData: any
  dataImageContent: any = []
  dataRelatedContent: any = []
  dataComment: any = []
  photoStatus: boolean = false
  shareUrlStatus: boolean = false;
  lengthArrImg: boolean = false;

  name: any
  comment: any
  commentStatus: boolean = false
  commentDisable: boolean = false
  contributorType: any = 3

  positionComment: any = 1
  commentStop: boolean = true

  constructor(
    public indonesiaTaxGuideDetailData: TaxRatesData,
    public navParams: NavParams,
    public alertCtrl: AlertController,
	public toastCtrl: ToastController,
    public socialSharing: SocialSharing,
    public navCtrl: NavController,
    private sanitizer: DomSanitizer
  ) {
    if(localStorage.getItem("login_token")){
      this.name = localStorage.getItem("name")
      this.commentDisable = true
      this.contributorType = 2
    }

    this.id = this.navParams.data.idContent
    this.getIndonesiaTaxGuideDetailData()
    this.getIndonesiaTaxGuideDetailDataImage()
    this.getIndonesiaTaxGuideDetailDataComment(1)
    this.getIndonesiaTaxGuideDetailDataRelated()
  }

  share(subject, url)
  {
    this.socialSharing.share(null, subject, null, url)
      .then(() =>{
        console.log("success sharing url")
      }).catch((err) => {
      console.log(err)
    })
  }

  ionViewWillEnter() {
  }

  getIndonesiaTaxGuideDetailData(){
    this.indonesiaTaxGuideDetailData.getItem(this.id)
      .subscribe(
        (data: any) => {
          if(data){
            this.detailData = data
            console.log(this.detailData.data.url_share)
            if(this.detailData.data.url_share != '') {
              this.shareUrlStatus = true
            }
          }
        },
        (err) => {
          console.log(err)
		  this.presentToast()
        }
    )
  }

  getIndonesiaTaxGuideDetailDataImage(){
    this.indonesiaTaxGuideDetailData.getItemImage(this.id)
      .subscribe(
        (data: any) => {
          if(data){
            this.dataImageContent = data.data;
            if (data.data.length == 1){
              this.lengthArrImg = true
            }
            this.photoStatus = true
          }
        },
        (err) => {
          console.log(err)
        }
    )
  }
  
	openHome() {
    this.navCtrl.push(HomePage)
  }
  
  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Koneksi Gagal",
      duration: 3000,
      position: "bottom",
      showCloseButton: true,
      closeButtonText: 'Coba lagi'
    });
    toast.present();

    toast.onDidDismiss(() => {
      this.openHome()
    });
  }
  
  getIndonesiaTaxGuideDetailDataComment(page){
    this.indonesiaTaxGuideDetailData.getItemComment(this.id,page)
      .subscribe(
        (data: any) => {
          if(data){
            if (page === 1) {
              this.dataComment = []
            }
            if(data.data.length === 0){
              this.commentStatus = false
            }
            this.dataComment = this.pushArray(this.dataComment, data.data)
          }
        },
        (err) => {
          console.log(err)
        }
    )
  }

  getIndonesiaTaxGuideDetailDataRelated(){
    this.indonesiaTaxGuideDetailData.getItemRelated(this.id)
      .subscribe(
        (data: any) => {
          if(data)
            this.dataRelatedContent = data.data
        },
        (err) => {
          console.log(err)
        }
    )
  }

  openSetting() {
    this.navCtrl.push(SettingPage)
  }

   postComment(creds){
    this.indonesiaTaxGuideDetailData.setItem(this.id, creds, false, "comment")
      .subscribe(data => {
          this.comment = ''
          this.commentStatus = false
          this.presentAlert("Terima Kasih, Komentar anda berhasil terkirim!")
          console.log(data)
      },
      err => {
        this.presentAlert(err)
        console.log(err)
      })
  }

  presentAlert(message) {
    let alert = this.alertCtrl.create({
      subTitle: message,
      buttons: ['ok']
    })
    alert.present();
  }

  sendComment(){
    if(this.name !== undefined && this.comment !== undefined){
      let comment
      comment = {}

      comment.type = 2
      comment.text_id = this.id
      comment.contributor_type = this.contributorType
      comment.contributor_id = localStorage.getItem("id")
      comment.alias = this.name
      comment.content = this.comment

      this.postComment(comment)
      console.log(comment)
    }else{
      this.presentAlert("Mohon lengkapi kolom nama dan komentar!")
    }
  }

  openComment(){
    this.commentStatus = true
  }

  closeComment(){
    this.commentStatus = false
  }

  goToRelated(idContent: any) {
    console.log(idContent);
    this.navCtrl.push(IndonesiaTaxGuideDetailPage, { idContent: idContent})
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
     this.doScroll();
      infiniteScroll.complete();
    }, 500);
  }

  doScroll(){
    if(this.commentStop){
      this.positionComment++
      this.getIndonesiaTaxGuideDetailDataComment(this.positionComment)
    }
  }

  pushArray(a:any, b:any){
    for(let i = 0; i < b.length; i++){
      a.push(b[i])
    }
    return a
  }

  toHTML(input) : any {
    let result = input.replace(/&amp;/g, "&").replace(/&quot;/g, '"').replace(/&lt;/g, "<").replace(/&gt;/g, ">");
    result = this.sanitizer.bypassSecurityTrustHtml(result);
    return result;
  }
}

