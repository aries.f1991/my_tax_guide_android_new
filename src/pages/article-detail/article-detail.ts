import { Component } from '@angular/core';
import { NavParams, NavController, ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { ArticleData } from '../../providers/article-data';
import { DomSanitizer } from '@angular/platform-browser';

import { HomePage } from '../home/home'


@Component({
  selector: 'page-session-detail',
  templateUrl: 'article-detail.html'
})

export class ArticleDetailPage {
  fontSize: any = localStorage.getItem('text_size') + 'px'
  id: any
  session: any
  detailData: any
  dataImageContent: any
  dataRelatedContent: any
  dataComment: any
  photoStatus: boolean = false;
  lengthArrImg: boolean = false;
  shareUrlStatus: boolean = false

  name: any
  comment: any
  commentStatus: boolean = false
  commentDisable: boolean = false
  contributorType: any = 3

  positionComment: any = 1
  commentStop: boolean = true

  public commentImage = "assets/img/home/comment.png"

  constructor(
    public articleData: ArticleData,
    public alertCtrl: AlertController,
    public navParams: NavParams,
	public toastCtrl: ToastController,
    public navCtrl: NavController,
    public socialSharing: SocialSharing,
    private sanitizer: DomSanitizer
  ) {
    if(localStorage.getItem("login_token")){
      this.name = localStorage.getItem("name")
      this.commentDisable = true
      this.contributorType = 2
    }

    console.log(this.fontSize)

    this.id = this.navParams.data.idContent
    this.getArticleData()
    this.getArticleDataComment(1)
    this.getArticleDataImage()
    this.getArticleDataRelated()

  }

  share(subject, url)
  {
    this.socialSharing.share(null, subject, null, url)
      .then(() =>{
        console.log("success sharing url")
      }).catch((err) => {
        console.log(err)
      })
  }

  ionViewWillEnter() {
  }

  getArticleData(){
    this.articleData.getItem(this.id)
      .subscribe(
        (data: any) => {
          if(data){
            this.detailData = data
            if(this.detailData.data.url_share != '') {
              this.shareUrlStatus = true
            }
            this.detailData.data.content = this.toHTML(this.detailData.data.content);
          }
        },
        (err) => {
          console.log(err)
		  this.presentToast()
        }
    )
  }

  getArticleDataImage(){
    this.articleData.getItemImage(this.id)
      .subscribe(
        (data: any) => {
          if(data){
            this.dataImageContent = data.data;
            if (data.data.length == 1){
              this.lengthArrImg = true
            }
            this.photoStatus = true
          }
        },
        (err) => {
          console.log(err)
        }
    )
  }

  getArticleDataComment(page){
    this.articleData.getItemComment(this.id,page)
      .subscribe(
        (data: any) => {
          if(data){
            if (page === 1) {
              this.dataComment = []
            }
            if(data.data.length === 0){
              this.commentStatus = false
            }
            this.dataComment = this.pushArray(this.dataComment, data.data)
          }
        },
        (err) => {
          console.log(err)
        }
    )
  }

  getArticleDataRelated(){
    this.articleData.getItemRelated(this.id)
      .subscribe(
        (data: any) => {
          if(data)
            this.dataRelatedContent = data.data
        },
        (err) => {
          console.log(err)
        }
    )
  }

  postComment(creds){
    this.articleData.setItem(this.id, creds, false, "comment")
      .subscribe(data => {
          this.comment = ''
          this.commentStatus = false
          this.presentAlert("Terima Kasih, Komentar anda berhasil terkirim!")
          console.log(data)
      },
      err => {
        this.presentAlert(err)
        console.log(err)
      })
  }

  presentAlert(message) {
    let alert = this.alertCtrl.create({
      subTitle: message,
      buttons: ['ok']
    })
    alert.present();
  }

  sendComment(){
    if(this.name !== undefined && this.comment !== undefined){
      let comment
      comment = {}

      comment.type = 1
      comment.text_id = this.id
      comment.contributor_type = this.contributorType
      comment.contributor_id = localStorage.getItem("id")
      comment.alias = this.name
      comment.content = this.comment

      this.postComment(comment)
      console.log(comment)
    }else{
      this.presentAlert("Mohon lengkapi kolom nama dan komentar!")
    }

  }

  openComment(){
    this.commentStatus = true
  }

  closeComment(){
    this.commentStatus = false
  }

  goToRelated(idContent: any) {
    console.log(idContent);
    this.navCtrl.push(ArticleDetailPage, { idContent: idContent})
  }

  openHome() {
    this.navCtrl.push(HomePage)
  }
  
  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Koneksi Gagal",
      duration: 3000,
      position: "bottom",
      showCloseButton: true,
      closeButtonText: 'Coba Lagi'
    });
    toast.present();

    toast.onDidDismiss(() => {
      this.openHome()
    });
  }
  
  doInfinite(infiniteScroll) {
    setTimeout(() => {
     this.doScroll();
      infiniteScroll.complete();
    }, 500);
  }

  doScroll(){
    if(this.commentStop){
      this.positionComment++
      this.getArticleDataComment(this.positionComment)
    }
  }

  pushArray(a:any, b:any){
    for(let i = 0; i < b.length; i++){
      a.push(b[i])
    }
    return a
  }

  toHTML(input) : any {
    let result = input.replace(/&amp;/g, "&").replace(/&quot;/g, '"').replace(/&lt;/g, "<").replace(/&gt;/g, ">");
    // result =  new DOMParser().parseFromString(result, "text/html").documentElement.textContent;
    result = this.sanitizer.bypassSecurityTrustHtml(result);
    return result;
  }

}

