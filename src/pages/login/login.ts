import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { NavController, LoadingController } from 'ionic-angular';
import { LoginRegisterData } from '../../providers/login-register-data'
import { UserData } from '../../providers/user-data';
import { GooglePlus } from '@ionic-native/google-plus';
import { AlertController } from 'ionic-angular';
import { Facebook } from '@ionic-native/facebook';

import { ForgotPasswordPage } from '../forgot-password/forgot-password'

@Component({
  selector: 'page-user',
  templateUrl: 'login.html'
})
export class LoginPage {
  private datas = {};
  private user_id:any;


  segment = 'login'

  // login: UserOptions = { username: '', password: '' };
  submitted = false;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public userData: UserData,
    public googlePlus: GooglePlus,
    public fb: Facebook,
    public loginRegisterData: LoginRegisterData,
    public alertCtrl: AlertController,
  ) {
  }

  googleLogin(){
  // let nav = this.navCtrl;
  let loading = this.loadingCtrl.create({
    content: 'Mohon tunggu...'
  });
  loading.present();
  this.googlePlus.login({
    'scopes': '', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
    'webClientId': '28361620506-5h6iv37pq4olke059lduslo3ddmtlupp.apps.googleusercontent.com', // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
    'offline': true
  })
  .then(res => {
    loading.dismiss();
    console.log(res)

    this.datas['email'] = res.email
    this.datas['name'] = res.displayName
    this.datas['url_photo'] = res.imageUrl;
    this.datas['type'] = 2;

    this.sentLogin(this.datas, "-login_social_media")
  })
  .catch(err=>{
    this.presentAlert("Error login with google : " + err)
    loading.dismiss()
  });
  }

  facebookLogin()
  {
    let loading = this.loadingCtrl.create({
      content: 'Mohon tunggu...'
    });

    loading.present();
    this.fb.login(['public_profile', 'user_friends', 'email'])
      .then(res =>{
        loading.dismiss();
        if(res.status == "connected")
        {
          this.user_id = res.authResponse.userID;
          this.getDetailFacebook();
        }
      })
      .catch(err=>{
        console.log("Gagal masuk dengan facebook : " + err)
        loading.dismiss()
      });
  }

  getDetailFacebook()
  {
    this.fb.api("/"+this.user_id+"/?fields=id,email,name,picture", ['public_profile'])
      .then(res=>{
        console.log(res)
        this.datas['email'] = res.email
        this.datas['name'] = res.name
        this.datas['url_photo'] = res.picture.data.url;
        this.datas['type'] = 3;

        this.sentLogin(this.datas, "-login_social_media")
      })
      .catch(err=>{
        console.log(err)
      });
  }


  presentAlert(msg)
  {
    let alert = this.alertCtrl.create({
      title: 'Info',
      message: msg,
      buttons: [
      {
        text: 'Ok',
        handler: () => {
          console.log('Button clicked');
        }
      }
      ]
    });
    alert.present();
  }

  onLogin(f: NgForm){
    let sent = f.value
    this.sentLogin(sent, "-login")
  }

  onRegister(f: NgForm){
    let sent = f.value
	if (sent.name === undefined)
		sent['name'] = ''
	if (sent.email === undefined)
		sent['email'] = ''
    this.sentRegister(sent)
  }

  sentLogin(sent, url) {
    console.log(sent)

    let loading = this.loadingCtrl.create({
      content: 'Mohon tunggu...'
    });

    loading.present();

    this.loginRegisterData.setItem(0, sent, false, url)
      .subscribe(data => {
        loading.dismiss()
          console.log(data)
          localStorage.setItem('type_login', sent['type'])
          console.log("type login : " + localStorage.getItem('type_login'))
          localStorage.setItem('login_token', data.login_token)
          localStorage.setItem('expired_token', data.expired_token)

          this.loginRegisterData.getItems('-get_data_profile')
            .subscribe(data => {
              console.log(data)

              localStorage.setItem('email', data.data.email)
              localStorage.setItem('name', data.data.name)
              localStorage.setItem('phone', data.data.phone)
              localStorage.setItem('dob', data.data.dob)
              localStorage.setItem('gender', data.data.gender)
              localStorage.setItem('profile_picture', data.data.profile_picture)
              localStorage.setItem('gender_text', data.data.gender_text)
              localStorage.setItem('address', data.data.address)
              localStorage.setItem('id', data.data.id)

              console.log(localStorage.getItem('email'))
              console.log(localStorage.getItem('name'))
              console.log(localStorage.getItem('phone'))
              console.log(localStorage.getItem('dob'))
              console.log(localStorage.getItem('gender'))
              console.log(localStorage.getItem('profile_picture'))
              console.log(localStorage.getItem('gender_text'))
              console.log(localStorage.getItem('address'))
              console.log(localStorage.getItem('id'))

              this.navCtrl.pop()
            })

      },
      err => {
        loading.dismiss()
		var data = JSON.parse(err._body)
        this.presentAlert("Gagal masuk. " + data.message)
        console.log(err)
      })
  }

  sentRegister(sent) {
    console.log(sent)

    let loading = this.loadingCtrl.create({
      content: 'Mohon tunggu...'
    });

    loading.present();
	var patt = new RegExp("[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})");
	if(patt.test(sent['email'])){
	if(sent['password'] === sent['confirm_password'])	
	{
    this.loginRegisterData.setItem(0, sent, false, "-registration")
      .subscribe(data => {
          loading.dismiss()
          console.log(data)
          this.presentAlert(data.message + ". Harap cek email untuk melakukan aktivasi.")
          this.navCtrl.pop()
      },
      err => {
        loading.dismiss()
		console.log(err)
		var data = JSON.parse(err._body)
        this.presentAlert("Gagal daftar. " + data.message)
        this.navCtrl.pop()
	})
	}
	else{
	loading.dismiss()
	this.presentAlert("Konfirmasi password tidak sama")
	this.navCtrl.pop()	
	}
	}
	else{
	loading.dismiss()
	this.presentAlert("E-mail tidak valid")
	this.navCtrl.pop()}
  }

  goToForgotPassword(){
    this.navCtrl.push(ForgotPasswordPage)
  }

  isActiveToggleTextPassword: Boolean = true;

  toggleTextPassword(): void{
      this.isActiveToggleTextPassword = !this.isActiveToggleTextPassword;
      console.log(this.isActiveToggleTextPassword)
  }

  getType() {
      return this.isActiveToggleTextPassword ? 'password' : 'text';
  }

}
