import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { Platform } from 'ionic-angular';
import { Http } from '@angular/http'
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { DataService } from '../../data.service'
import { AlertController, LoadingController } from 'ionic-angular';

declare var google: any;

@Component({
  selector: 'page-tax-office-location',
  templateUrl: 'tax-office-location.html'
})
export class TaxOfficeLocationPage {
  destination:string
  start:string
  map: any
  public putarArah = "assets/img/putar.png"

  position:any = []
  resultData = []
  taxOffice = []

  public iconBase = 'assets/img/tax-office-location/tax-icon.png'
  public iconSelected = 'assets/img/tax-office-location/tax-icon-selected.png'

  @ViewChild('mapCanvas') mapElement: ElementRef
  constructor(
    public http: Http,
    public ds: DataService,
    public geolocation: Geolocation,
    public platform: Platform,
    public ngZone:NgZone,
    private launchNavigator: LaunchNavigator,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController
  ) {
    this.start = ""

    let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
    this.geolocation.getCurrentPosition().then((position) => {
      loading.dismiss()
      this.position = position
      this.getNearTaxOfficeLocation(position)
      this.loadMap(position)
      this.taxOffice = this.resultData
    })
  }

  ionViewDidLoad() {
  }

  loadMap(position){
    let mapEle = this.mapElement.nativeElement

    let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
    let mapOptions = {
      center: latLng,
      zoom: 12,
	  fullscreenControl: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }   
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions)

    new google.maps.Marker({
      position: latLng,
      map: this.map,
      title: "Lokasi Anda"
    });

    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      mapEle.classList.add('show-map')
    });
  }

  loadTaxOfficesMarker(){
    for(var i = 0; i < this.taxOffice.length; i++)
      this.addMarker(this.taxOffice[i],false)
  }

  addMarker(data, selected){
    let icon
    if(selected)
      icon = this.iconSelected
    else
      icon = this.iconBase

    let marker = new google.maps.Marker({
      position: data,
      icon : icon,
      map: this.map,
      title: data.name
    });

    google.maps.event.addListener(marker, 'click', () => {
      //Call run function to set the data within angular zone to trigger change detection.
      this.ngZone.run(()=>{
        if(selected)
          this.listernerClickIconSelected(data);
        else
          this.listernerClickIcon(data);
      });
    })
  }

  directionConfirm(data) {
    let alert = this.alertCtrl.create({
      title: 'Pergi ke Kantor Pajak',
      message: 'Apa anda ingin ke ' + data.name + '?',
      buttons: [
        {
          text: 'Tidak',
          role: 'cancel',
          handler: () => {
            
          }
        },
        {
          text: 'Iya',
          handler: () => {
            this.destination = data.lat + ', ' + data.lng
            this.navigate()
          }
        }
      ]
    });
    alert.present();
  }

  listernerClickIcon(data){
    this.loadMap(this.position)
    this.addMarker(data, true)

    this.taxOffice = []
    this.taxOffice[0] = data 
  }

  listernerClickIconSelected(data){
    this.directionConfirm(data)
  }
  
  getNearTaxOfficeLocation(position){
    let creds = position.coords.latitude + ',' + position.coords.longitude
    this.http.get(this.ds.url['tax-office-location'] + creds).map(res => res.json()).subscribe(data => {
        this.processData(data,creds)
    });
  }

  navigate(){
    let options: LaunchNavigatorOptions = {
      start: this.start
    };

    this.launchNavigator.navigate(this.destination, options);
  }

  processData(data,creds){
    let _data = []
    const length = data.results.length
    
    let destinationCreds = ''

    for(var i=0; i < length; i++){
      destinationCreds = destinationCreds + 
        data.results[i].geometry.location.lat + ',' + 
        data.results[i].geometry.location.lng 

      if(i != length - 1)
        destinationCreds = destinationCreds + '|'
    }
    
    this.http.get(this.ds.url['tax-office-location' + this.ds.distance] + this.ds.origins + creds + this.ds.destinations + destinationCreds).map(res => res.json()).subscribe(dataGet => {

      for(i=0; i < length; i++){
        var obj = { 
          id       : data.results[i].id, 
          name     : data.results[i].name,
          address  : data.results[i].formatted_address,
          lat      : data.results[i].geometry.location.lat,
          lng      : data.results[i].geometry.location.lng,
          distance : dataGet.rows[0].elements[i].distance.text,
          long     : dataGet.rows[0].elements[i].distance.value
        }
        _data[i] = obj
      }

      _data.sort(function(a,b) {
        return a['long']-b['long']
      });
  
      for(i=0; i < 3; i++){
        this.resultData[i] = _data[i]    
      }
      
      this.taxOffice = this.resultData
      this.loadTaxOfficesMarker()
    });
  }
}
