import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { NavController, LoadingController } from 'ionic-angular';
import { LoginRegisterData } from '../../providers/login-register-data'
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html'
})
export class ForgotPasswordPage {
  public email

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public loginRegisterData: LoginRegisterData,
    public alertCtrl: AlertController,
  ) {
    //ubah disini bet
    this.email = localStorage.getItem('email')
  }

  presentAlert(msg){
    let alert = this.alertCtrl.create({
      title: 'Info',
      message: msg,
      buttons: [
      {
        text: 'Ok',
        handler: () => {
          console.log('Button clicked');
        }
      }
      ]
    });
    alert.present();
  }

  onForgotPassword(f: NgForm){
    let sent = f.value
    //cek datanya disini yaa
    console.log("Data siap kirim : " + sent)
    this.resetPassword(sent)
  }

  resetPassword(sent) {
    console.log(sent)
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();
    // ganti end point buat edit profile
    this.loginRegisterData.setItem(this.email, sent, false, "-forgot_password")
      .subscribe(data => {
          loading.dismiss()
          console.log(data)
          //masukin ke localstorage lagi data usernya kalau udah berhasil
          this.presentAlert(data.message + ". Cek e-mail anda untuk password baru.")
          this.navCtrl.pop()
      },
      err => {
        this.presentAlert("Gagal mengirimkan password baru.")
        console.log(err)
        loading.dismiss()
      })
  }

}