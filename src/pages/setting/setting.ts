import { Component } from '@angular/core';
import { NavParams, NavController, AlertController } from 'ionic-angular';

import { ConferenceData } from '../../providers/conference-data';

import { EditProfilePage } from '../edit-profile/edit-profile'
import { ChangePasswordPage } from '../change-password/change-password'
import { LoginPage } from '../login/login'

@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html'
})
export class SettingPage {
	public is_signIn = false;
  public is_sosmed = false;
	public base64Image: any;
  public name: any;
  public picture : any;

	constructor(
	   public dataProvider: ConferenceData,
      public navParams: NavParams,
      public navCtrl: NavController,
      public alertCtrl: AlertController
	) {
  }

  ionViewWillEnter() {
    if(localStorage.getItem('login_token') == null)
      this.is_signIn = false
    else
    {
      this.is_signIn = true
      this.name = localStorage.getItem('name')
      this.picture = localStorage.getItem('profile_picture')
      
      if(localStorage.getItem('type_login') == 'undefined')
      {
        console.log("masuk salah")
        this.is_sosmed = false
      }
      else
      {
        console.log("type login : " + localStorage.getItem('type_login'))
        console.log("masuk benar")
        this.is_sosmed = true
      }
    }



	}

  adjustTextSize()
  {
    let size = localStorage.getItem('text_size')
    let this_20 = false
    let this_14 = false
    let this_10 = false

    if(size == '20')
      this_20 = true
    else if(size == '14')
      this_14 = true
    else if(size == '10')
      this_10 = true

    let alert = this.alertCtrl.create({
      title: 'Ukuran Teks',
      inputs : [
      {
        type:'radio',
        label: 'Besar',
        value: '20',
        checked: this_20
      },
      {
        type:'radio',
        label: 'Normal',
        value: '14',
        checked: this_14
      },
      {
        type:'radio',
        label: 'Kecil',
        value: '10',
        checked: this_10
      }
      ],
      buttons: [
      {
        text: 'Ok',
        handler: (data:string) => {
          console.log(data);
          localStorage.setItem('text_size', data)
        }
      }
      ]
    });
    alert.present();
  }

	signOut(){
    localStorage.removeItem('login_token')
    this.picture = null
    this.is_signIn = false
    this.is_sosmed = false
    localStorage.removeItem('type_login')
    localStorage.removeItem('email')
    localStorage.removeItem('name')
    localStorage.removeItem('phone')
    localStorage.removeItem('dob')
    localStorage.removeItem('gender')
    localStorage.removeItem('profile_picture')
    localStorage.removeItem('gender_text')
    localStorage.removeItem('address')
    localStorage.removeItem('id')
	}

  goToEditProfil(){
    this.navCtrl.push(EditProfilePage)
  }

  goToChangePassword(){
    this.navCtrl.push(ChangePasswordPage)
  }

  goToSignIn(){
    this.navCtrl.push(LoginPage)
  }

}
