import { AlertController, App, ModalController, NavController, ToastController, LoadingController, Slides, Platform } from 'ionic-angular'
import { Component, ViewChild } from '@angular/core'

import { ArticleDetailPage } from '../article-detail/article-detail'
import { NewsDetailPage } from '../news-detail/news-detail'
import { SettingPage} from "../setting/setting"

import { ArticleData } from '../../providers/article-data'
import { NewsData } from '../../providers/news-data'
import { UserData } from '../../providers/user-data'
import { SlideData } from '../../providers/slider-data'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public stateSearch = true
  public newsImage = "assets/img/home/newspaper-icon-9.png"
  public articleImage = "assets/img/home/article.png"
  public commentImage = "assets/img/home/comment.png"
  public arrowImage = "assets/img/arrow.png"

  @ViewChild('slides') slides: Slides
  page1: any = ArticleDetailPage
  page2: any = NewsDetailPage

  segment = 'news'
  groups: any = []
  confDate: string

  marginSearchShow: any = '10px';

  listSlide: any = [
    {
      "id": "0",
      "type_id": "0",
      "title": "Data not load",
      "date_created": "Today",
      "primary_image": "assets/img/placeholder-image.png"
    }
  ];

  totalArticle: any
  positionPageArticle: any = 1
  listArticle: any = [{
    "id": "0",
    "title": "Data not load",
    "tag": "Data not load",
    "url_share": "",
    "total_viewer": "1",
    "date_created": "1 week ago",
    "total_comment": 1,
    "primary_image": "assets/img/placeholder-image.png"
  }]

  totalNews: any
  positionPageNews: any = 1
  listNews: any = [
    {
      "id": "0",
      "title": "Data not load",
      "tag": "Data not load",
      "url_share": "",
      "total_viewer": "1",
      "date_created": "1 week ago",
      "total_comment": 1,
      "primary_image": "assets/img/placeholder-image.png"
    }
  ]

  constructor(
    public alertCtrl: AlertController,
    public app: App,
	public platform: Platform,

    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public toastCtrl: ToastController,

    public user: UserData,
    public newsData: NewsData,
    public sliderData: SlideData,
    public articleData: ArticleData
  ) {
    if(!localStorage.getItem('token'))
      this.user.valueToken()

    this.getDataSlider()
    this.getDataArticle(1)
    this.getDataNews(1)
  }

  ionViewDidLoad() {
    this.slides.update()
    this.app.setTitle('Home')
  }
  
  ionViewWillEnter() {
    this.slides.update()
  }

  getDataSlider() {
    let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  loading.present();
    this.sliderData.getItems([])
      .subscribe(
          (data: any) => {
            loading.dismiss()
            if(data){
              this.listSlide = []
              this.listSlide = data.data
            }
          },
          (err) => {
			loading.dismiss()
            console.log(err)
			this.presentToast()
          }
      )
  }

  onClearFunction(event){
    console.log(event);
    if (this.segment == "article"){
      this.getDataArticle(1)
    } else if (this.segment == "news"){
      this.getDataNews(1)
    }
  }

  
  searchFunction(event){
    let input = event.target.value

    if (this.segment == "article"){
      this.articleData.getItemsSearch(input)
        .subscribe(
          (data: any) => {
            this.listArticle = []
            this.listArticle = data.data
          },
          (err) => {
            console.log(err)
          }
        )
    } else  if (this.segment == "news"){
      this.newsData.getItemsSearch(input)
        .subscribe(
          (data: any) => {
            this.listNews = [];
            this.listNews = data.data
          },
          (err) => {
            console.log(err)
          }
        )
    }
  }
  
  getDataArticle(page) {
    this.articleData.getItems(page)
        .subscribe(
            (data: any) => {
              if (page === 1) {
                this.listArticle = []
              }
              this.listArticle = this.pushArray(this.listArticle, data.data)
              this.totalArticle = data.total_article
			  
            },
            (err) => {
              console.log(err)
            }
        )
  }

  getDataNews(page) {
    this.newsData.getItems(page)
        .subscribe(
            (data: any) => {
              if (page === 1) {
                this.listNews = []
              }
              this.listNews = this.pushArray(this.listNews, data.data)
              this.totalNews = data.total_news
              console.log(this.listNews)
            },
            (err) => {
              console.log(err)
            }
        )
  }

  onSearch(message){
    if(this.segment === 'news'){
      this.newsData.getItemsSearch(message)
        .subscribe(
            (data: any) => {
              this.listNews = data.data
            },
            (err) => {
              console.log(err)
            }
        )
    }else {
      this.articleData.getItemsSearch(message)
        .subscribe(
            (data: any) => {
              this.listArticle = data.data
            },
            (err) => {
              console.log(err)
            }
        )
    }
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
     this.doScroll();
      infiniteScroll.complete();
    }, 500);
  }

  doRefresh(){
    this.getDataSlider()
    this.listArticle = []
    this.positionPageArticle = 1
    this.getDataArticle(1)
    this.listNews = []
    this.positionPageNews = 1
    this.getDataNews(1)
  }

  doScroll(){
    console.log(this.segment)
    if(this.segment === 'news'){
      let max = this.totalNews / 10
      if(this.positionPageNews < max){
        this.positionPageNews++
        this.getDataNews(this.positionPageNews)
      }
    }else {
      let max = this.totalArticle / 10
      if(this.positionPageArticle < max){
        this.positionPageArticle++
        this.getDataArticle(this.positionPageArticle)
      }
    }
  }

  pushArray(a:any, b:any){
    for(let i = 0; i < b.length; i++){
      a.push(b[i])
    }
    return a
  }

  openSearch() {
    this.stateSearch = !this.stateSearch;
    if (!this.stateSearch){
      this.marginSearchShow = '65px'
    } else {
      this.marginSearchShow = '10px'
    }
  }

  openSetting() {
    this.navCtrl.push(SettingPage)
  }

  startApp() {
    this.navCtrl.setRoot(HomePage)
  }

  goToDetail(idContent: any) {
    if(this.segment === 'news'){
      this.navCtrl.push(NewsDetailPage, { idContent: idContent })
    } else {
      this.navCtrl.push(ArticleDetailPage, { idContent: idContent })
    }
  }

  goToDetailArticle(idContent: any) {
      this.navCtrl.push(ArticleDetailPage, { idContent: idContent })
  }
  goToDetailNews(idContent: any) {
    this.navCtrl.push(NewsDetailPage, { idContent: idContent })
  }
  
  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Koneksi Gagal",
      duration: 3000,
      position: "bottom",
      showCloseButton: true,
      closeButtonText: 'Coba Lagi'
    });
    toast.present();

    toast.onDidDismiss(() => {
      this.doRefresh()
    });
  }
}
