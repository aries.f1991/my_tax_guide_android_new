import { Component, ViewChild } from '@angular/core';

import { MenuController, NavController, Slides } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { HomePage } from '../home/home';

@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html'
})

export class TutorialPage {
  showSkip = true;

  public className;
  public className1 = "slide-1";
  public className2 = "slide-2";
  public className3 = "slide-3";
  public className4 = "slide-4";
  public dataImage: any = [];
  public timeSlide: any = 5000;

  @ViewChild('slides') slides: Slides;

  constructor(
    public navCtrl: NavController,
    public menu: MenuController,
    public storage: Storage
  ) {
    this.className = this.className1
    this.dataImage = [
      {
        img : "assets/img/splashScreen.jpg",
        end : false
      },
      {
        img : "assets/img/landing2.jpg",
        end : false
      },
      {
        img : "assets/img/landing3.jpg",
        end : false
      },
      {
        img : "assets/img/landing4.jpg",
        end : true
      },
    ]
    this.timeSlide = 5000
  }

  startApp() {
    this.storage.set('hasSeenTutorial', 'true');
    this.navCtrl.setRoot(HomePage)
  }

  onSlideChangeStart(slider: Slides) {
    this.showSkip = !slider.isEnd();
    switch(this.slides.getActiveIndex()) {
      case 0: {
        this.className = this.className1
        break;
      }
      case 1: {
        this.className = this.className2
        break;
      }
      case 2: {
        this.className = this.className3
        break;
      }
      case 3: {
        this.className = this.className4;
        this.timeSlide = 10000000;
        break;
      }
      default: {
        this.className = this.className4
        break;
      }
    }
  }

  ionViewWillEnter() {
    this.slides.update();
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }

  ionViewDidLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }

}
