import { Component } from '@angular/core';
import { NavParams, NavController, LoadingController, ToastController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { NewsData } from '../../providers/news-data';
import { HomePage } from '../home/home'

import { TaxCustomRegulationData } from '../../providers/tax-custom-regulation-data'
import { AlertController } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'page-tax-and-custom-regulation-detail',
  templateUrl: 'tax-and-custom-regulation-detail.html'
})
export class TaxAndCustomRegulationDetailPage {
  fontSize: any = localStorage.getItem('text_size') + 'px'
  id: any
  session: any
  detailData: any
  dataImageContent: any
  dataRelatedContent: any
  dataComment: any
  photoStatus: boolean = false
  shareUrlStatus: boolean = false

  name: any
  comment: any
  commentStatus: boolean = false
  commentDisable: boolean = false
  contributorType: any = 3

  positionComment: any = 1
  commentStop: boolean = true

  public commentImage = "assets/img/home/comment.png"
  public stateSearch = true
  marginSearchShow: any = '10px';

  constructor(
    public newsData: NewsData,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public navCtrl: NavController,
	public toastCtrl: ToastController,
    public socialSharing: SocialSharing,
    private sanitizer: DomSanitizer,
    private loadingCtrl: LoadingController,
    public taxRegulationData: TaxCustomRegulationData
  ) {
    // if(localStorage.getItem("login_token")){
    //   this.name = localStorage.getItem("name")
    //   this.commentDisable = true
    //   this.contributorType = 2
    // }

    this.id = this.navParams.data.id
    // this.getNewsDataImage()
    // this.getNewsDataComment(1)
    // this.getNewsDataRelated()
  }

  share(subject, url)
  {
    this.socialSharing.share(null, subject, null, url)
      .then(() =>{
        console.log("success sharing url")
      }).catch((err) => {
        console.log(err)
      });
  }

  ionViewWillEnter() {
    this.getData()
  }

  getData(){
    let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  loading.present();
    this.taxRegulationData.nameClass = 'kurs-pajak-detailTaxRegulation'
    this.taxRegulationData.getDetail(this.id)
      .subscribe(
        (data: any) => {
          loading.dismiss()
          if(data){
            this.detailData = data
            if(this.detailData.data.url_share != '') {
              this.shareUrlStatus = true
            }
            this.detailData.data.content = this.toHTML(this.detailData.data.content);
          }
        },
        (err) => {
          loading.dismiss()
          console.log(err)
		  this.presentToast()
        }
    )
  }
  
  openHome() {
    this.navCtrl.push(HomePage)
  }
  
  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Koneksi Gagal",
      duration: 3000,
      position: "bottom",
      showCloseButton: true,
      closeButtonText: 'Coba Lagi'
    });
    toast.present();

    toast.onDidDismiss(() => {
      this.openHome()
    });
  }

  presentAlert(message) {
    let alert = this.alertCtrl.create({
      subTitle: message,
      buttons: ['ok']
    })
    alert.present();
  }

  pushArray(a:any, b:any){
    for(let i = 0; i < b.length; i++){
      a.push(b[i])
    }
    return a
  }

  toHTML(input) : any {
    let result = input.replace(/&amp;/g, "&").replace(/&quot;/g, '"').replace(/&lt;/g, "<").replace(/&gt;/g, ">");
    // result =  new DOMParser().parseFromString(result, "text/html").documentElement.textContent;
    result = this.sanitizer.bypassSecurityTrustHtml(result);
    return result;
  }
}

