import { Component } from '@angular/core'
import { NavController, NavParams, ToastController } from 'ionic-angular'
import { SocialSharing } from '@ionic-native/social-sharing'
import { SettingPage } from "../setting/setting"
import { AlertController } from 'ionic-angular';
import { SeminarData } from '../../providers/seminar-data'

import { CustomSeminarRegisterPage } from '../custom-seminar-register/custom-seminar-register'
import { HomePage } from '../home/home'

@Component({
  selector: 'page-custom-seminar-detail',
  templateUrl: 'custom-seminar-detail.html'
})

export class CustomSeminarDetailPage {
  fontSize: any = localStorage.getItem('text_size') + 'px'
  id: any
  session: any
  detailData: any
  dataImageContent: any = []
  dataComment: any = []
  photoStatus: boolean = false
  shareUrlStatus: boolean = false
  pendaftar: any = 1;
  lengthArrImg: boolean = false;

  name: any
  comment: any
  commentStatus: boolean = false
  commentDisable: boolean = false
  contributorType: any = 3

  positionComment: any = 1
  commentStop: boolean = true

  constructor(
    public customSeminarDetailData: SeminarData,
    public navParams: NavParams,
	public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public socialSharing: SocialSharing,
    public navCtrl: NavController
  ) {
    if(localStorage.getItem("login_token")){
      this.name = localStorage.getItem("name")
      this.commentDisable = true
      this.contributorType = 2
    }

    this.id = this.navParams.data.idContent
    this.getcustomSeminarDetailData()
    this.getcustomSeminarDetailDataImage()
    this.getcustomSeminarDetailDataComment(1)
  }

  share(subject, url)
  {
    this.socialSharing.share(null, subject, null, url)
      .then(() =>{
        console.log("success sharing url")
      }).catch((err) => {
      console.log(err)
    })
  }

  ionViewWillEnter() {
  }

  getcustomSeminarDetailData(){
    this.customSeminarDetailData.getItem(this.id)
      .subscribe(
        (data: any) => {
          if(data){
            this.detailData = data
            console.log(this.detailData.data.url_share)
            if(this.detailData.data.url_share != '') {
              this.shareUrlStatus = true
            }
          }
        },
        (err) => {
          console.log(err)
		  this.presentToast()
        }
    )
  }

  getcustomSeminarDetailDataImage(){
    this.customSeminarDetailData.getItemImage(this.id)
      .subscribe(
        (data: any) => {
          if(data){
            this.dataImageContent = data.data;
            if (data.data.length == 1){
              this.lengthArrImg = true
            }
            this.photoStatus = true
          }
        },
        (err) => {
          console.log(err)
        }
    )
  }

  getcustomSeminarDetailDataComment(page){
    this.customSeminarDetailData.getItemComment(this.id,page)
      .subscribe(
        (data: any) => {
          if(data){
            if (page === 1) {
              this.dataComment = []
            }
            if(data.data.length === 0){
              this.commentStatus = false
            }
            this.dataComment = this.pushArray(this.dataComment, data.data)
          }
        },
        (err) => {
          console.log(err)
        }
    )
  }

  openSetting() {
    this.navCtrl.push(SettingPage)
  }

   postComment(creds){
    this.customSeminarDetailData.setItem(this.id, creds, false, "comment")
      .subscribe(data => {
          this.comment = ''
          this.commentStatus = false
          this.presentAlert("Terima Kasih, Komentar anda berhasil terkirim!")
          console.log(data)
      },
      err => {
        this.presentAlert(err)
        console.log(err)
      })
  }

  presentAlert(message) {
    let alert = this.alertCtrl.create({
      subTitle: message,
      buttons: ['ok']
    })
    alert.present();
  }
	
  openHome() {
    this.navCtrl.push(HomePage)
  }
  
  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Koneksi Gagal",
      duration: 3000,
      position: "bottom",
      showCloseButton: true,
      closeButtonText: 'Coba Lagi'
    });
    toast.present();

    toast.onDidDismiss(() => {
      this.openHome()
    });
  }
  
  sendComment(){
    if(this.name !== undefined && this.comment !== undefined){
      let comment
      comment = {}

      comment.type = 2
      comment.text_id = this.id
      comment.contributor_type = this.contributorType
      comment.contributor_id = localStorage.getItem("id")
      comment.alias = this.name
      comment.content = this.comment

      this.postComment(comment)
      console.log(comment)
    }else{
      this.presentAlert("Mohon lengkapi kolom nama dan komentar!")
    }
  }

  openComment(){
    this.commentStatus = true
  }

  closeComment(){
    this.commentStatus = false
  }

  goToRelated(idContent: any) {
    console.log(idContent);
    this.navCtrl.push(CustomSeminarDetailPage, { idContent: idContent})
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
     this.doScroll();
      infiniteScroll.complete();
    }, 500);
  }

  doScroll(){
    if(this.commentStop){
      this.positionComment++
      this.getcustomSeminarDetailDataComment(this.positionComment)
    }
  }

  pushArray(a:any, b:any){
    for(let i = 0; i < b.length; i++){
      a.push(b[i])
    }
    return a
  }

  toHTML(input) : any {
    return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
  }

  goToRegister(){
    console.log(this.detailData)
    this.navCtrl.push(CustomSeminarRegisterPage, { idContent:this.id, user:this.pendaftar, startdate:this.detailData.data.pendaftaran_start, enddate:this.detailData.data.pendaftaran_end })
  }

  statusDateConvert(dateClose){
    var inputDate = new Date(dateClose);
    var todayDate = new Date();

    if(inputDate.setHours(0,0,0,0) < todayDate.setHours(0,0,0,0)) {
      return "Close"
    }else{
      return "Open"
    }
  }

  convertDateText(date){
    var inputDate = new Date(date)
    var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ]

    var day = inputDate.getDate()
    var monthIndex = inputDate.getMonth()
    var year = inputDate.getFullYear()

    return day + ' ' + monthNames[monthIndex] + ' ' + year
  }
}

