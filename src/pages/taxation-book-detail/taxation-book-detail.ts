import { Component } from '@angular/core'
import { NavController, NavParams, ToastController} from 'ionic-angular'
import { SocialSharing } from '@ionic-native/social-sharing'
import { SettingPage } from "../setting/setting"
import { TaxBooksData } from '../../providers/tax-books-data'
import { AlertController } from 'ionic-angular';
import {DomSanitizer} from "@angular/platform-browser";
import { HomePage } from '../home/home'

@Component({
  selector: 'page-taxation-book-detail',
  templateUrl: 'taxation-book-detail.html'
})

export class TaxationBookDetailPage {
  fontSize: any = localStorage.getItem('text_size') + 'px'
  id: any
  session: any
  detailData: any
  dataImageContent: any
  dataRelatedContent: any
  dataComment: any
  photoStatus: boolean = false
  shareUrlStatus: boolean = false;
  lengthArrImg: boolean = false;

  name: any
  comment: any
  commentStatus: boolean = false
  commentDisable: boolean = false
  contributorType: any = 3

  constructor(
    public taxBooksData: TaxBooksData,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public socialSharing: SocialSharing,
	public toastCtrl: ToastController,
    public navCtrl: NavController,
    // public content: Content,
    private sanitizer: DomSanitizer
  ) {
    if(localStorage.getItem("login_token")){
      this.name = localStorage.getItem("name")
      this.commentDisable = true
      this.contributorType = 2
    }

    this.id = this.navParams.data.idContent
    this.getTaxBooksDetailData()
    this.getTaxBooksDetailDataImage()
    this.getTaxBooksDetailDataComment(1)
  }

  share(subject, url)
  {
    this.socialSharing.share(null, subject, null, url)
      .then(() =>{
        console.log("success sharing url")
      }).catch((err) => {
      console.log(err)
    })
  }

  ionViewWillEnter() {
  }

  getTaxBooksDetailData(){
    this.taxBooksData.getItem(this.id)
      .subscribe(
        (data: any) => {
          if(data){
            this.detailData = data
            if(this.detailData.data.url_share != '') {
              this.shareUrlStatus = true
            }
          }
        },
        (err) => {
          console.log(err)
		  this.presentToast()
        }
    )
  }
  
  openHome() {
    this.navCtrl.push(HomePage)
  }
  
  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Koneksi Gagal",
      duration: 3000,
      position: "bottom",
      showCloseButton: true,
      closeButtonText: 'Coba Lagi'
    });
    toast.present();

    toast.onDidDismiss(() => {
      this.openHome()
    });
  }

  getTaxBooksDetailDataImage(){
    this.taxBooksData.getItemImage(this.id)
      .subscribe(
        (data: any) => {
          if(data){
            this.dataImageContent = data.data;
            if (data.data.length == 1){
              this.lengthArrImg = true
            }
            this.photoStatus = true
          }
        },
        (err) => {
          console.log(err)
        }
    )
  }

  getTaxBooksDetailDataComment(page){
    this.taxBooksData.getItemComment(this.id,page)
      .subscribe(
        (data: any) => {
          if(data)
            this.dataComment = data.data
        },
        (err) => {
          console.log(err)
        }
    )
  }

   postComment(creds){
    this.taxBooksData.setItem(this.id, creds, false, "comment")
      .subscribe(data => {
          this.comment = ''
          this.commentStatus = false
          this.presentAlert("Terima Kasih, Komentar anda berhasil terkirim!")
          console.log(data)
      },
      err => {
        this.presentAlert(err)
        console.log(err)
      })
  }

  presentAlert(message) {
    let alert = this.alertCtrl.create({
      subTitle: message,
      buttons: ['ok']
    })
    alert.present();
  }

  sendComment(){
    if(this.name !== undefined && this.comment !== undefined){
      let comment
      comment = {}

      comment.type = 4
      comment.text_id = this.id
      comment.contributor_type = this.contributorType
      comment.contributor_id = localStorage.getItem("id")
      comment.alias = this.name
      comment.content = this.comment

      this.postComment(comment)
      console.log(comment)
    }else{
      this.presentAlert("Mohon lengkapi kolom nama dan komentar!")
    }

  }

  openComment(){
    this.commentStatus = true
  }

  closeComment(){
    this.commentStatus = false
  }

  openSetting() {
    this.navCtrl.push(SettingPage)
  }

  toHTML(input) : any {
    let result = input.replace(/&amp;/g, "&").replace(/&quot;/g, '"').replace(/&lt;/g, "<").replace(/&gt;/g, ">");
    result = this.sanitizer.bypassSecurityTrustHtml(result);
    return result;
  }

  // toBottom(){
  //   // this.content.scrollToBottom(300)
  // }
}

