import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { NavController, LoadingController } from 'ionic-angular';
import { LoginRegisterData } from '../../providers/login-register-data'
import { UserData } from '../../providers/user-data';
import { GooglePlus } from '@ionic-native/google-plus';
import { AlertController } from 'ionic-angular';
import { Facebook } from '@ionic-native/facebook';
import { Camera } from '@ionic-native/camera';
import { ActionSheetController } from 'ionic-angular'

@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html'
})
export class EditProfilePage {
  private datas: any = {}
  private user_id: any
  private dataImage: any

  public nameEditProfil
  public emailEditProfil
  public dobEditProfil
  public phoneEditProfil
  public addressEditProfil
  public genderEditProfil

  public picture

  submitted = false;

  constructor(
    public actionSheetCtrl: ActionSheetController,
    public navCtrl: NavController,
    public loadingCtrl: LoadingController, 
    public userData: UserData, 
    public googlePlus: GooglePlus, 
    public fb: Facebook,
    public loginRegisterData: LoginRegisterData,
    public alertCtrl: AlertController,
    public camera : Camera
  ) {
  }

  ionViewWillEnter()
  {
    this.getData()
    this.picture = localStorage.getItem('profile_picture')
    this.nameEditProfil = localStorage.getItem('name')
    this.emailEditProfil = localStorage.getItem('email')
    let dob: any

    try{
      dob = new Date(localStorage.getItem('dob')).toISOString()
    }
    catch(e){
      console.log(e)
      dob = ""
    }

    this.dobEditProfil = dob 
    this.phoneEditProfil = localStorage.getItem('phone')
    this.addressEditProfil = localStorage.getItem('address')
    this.genderEditProfil = localStorage.getItem('gender')
  }

  presentAlert(msg){
    let alert = this.alertCtrl.create({
      title: 'Info',
      message: msg,
      buttons: [
      {
        text: 'Ok',
        handler: () => {
          console.log('Button clicked');
        }
      }
      ]
    });
    alert.present();
  }

  onEditProfil(f: NgForm){
    let sent = f.value
    //cek datanya disini yaa
    console.log(sent)
    this.editProfile(sent)
  }

  editProfile(sent) {
    console.log(sent)
    let loading = this.loadingCtrl.create({
      content: 'Mohon tunggu...'
    });

    loading.present();

    this.datas['image'] = this.dataImage
	
	if(this.datas['image']){
    console.log(this.datas)
    this.loginRegisterData.setItem(this.user_id, this.datas, false, "-update_profile_picture")
      .subscribe(data => {
        console.log(data)
        // ganti end point buat edit profile
        this.loginRegisterData.setItem(this.user_id, sent, false, "-edit_profile")
          .subscribe(data => {
              loading.dismiss();
              console.log(data)
              //masukin ke localstorage lagi data usernya kalau udah berhasil
              this.presentAlert(data.message)
              this.getData()
              this.navCtrl.pop()
          },
          err => {
            this.presentAlert("Gagal mengganti gambar profil")
            loading.dismiss()
            console.log(err)
          })
      },
      err => {
        this.presentAlert("Gagal mengganti gambar profil")
        console.log(err)
        loading.dismiss()
	})}
	else
	{
		this.loginRegisterData.setItem(this.user_id, sent, false, "-edit_profile")
          .subscribe(data => {
              loading.dismiss();
              console.log(data)
              //masukin ke localstorage lagi data usernya kalau udah berhasil
              this.presentAlert(data.message)
              this.getData()
              this.navCtrl.pop()
          },
          err => {
            this.presentAlert("Gagal mengubah profil")
            loading.dismiss()
            console.log(err)
          })
	}
  }

  takePictureGallery()
  {
    this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 1000,
        targetHeight: 1000,
        sourceType: 0
    }).then((imageData) => {
      // imageData is a base64 encoded string
        this.dataImage = imageData
        this.picture = "data:image/jpeg;base64," + imageData;
    }, (err) => {
        this.presentAlert(err);
    });
  }

  takePictureCamera()
  {
    this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 1000,
        targetHeight: 1000,
        sourceType: 1
    }).then((imageData) => {
      // imageData is a base64 encoded string
        this.dataImage = imageData
        this.picture = "data:image/jpeg;base64," + imageData;
    }, (err) => {
        this.presentAlert(err);
    });
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Choose source',
      buttons: [
        {
          text: 'Camera',
          handler: () => {
            this.takePictureCamera()
            console.log('Camera clicked');
          }
        },{
          text: 'Gallery',
          handler: () => {
            this.takePictureGallery()
            console.log('Gallery clicked');
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }


  getData()
  {
    this.loginRegisterData.getItems('-get_data_profile')
            .subscribe(data => {
              console.log(data)

              localStorage.setItem('email', data.data.email)
              localStorage.setItem('name', data.data.name)
              localStorage.setItem('phone', data.data.phone)
              localStorage.setItem('dob', data.data.dob)
              localStorage.setItem('gender', data.data.gender)
              localStorage.setItem('profile_picture', data.data.profile_picture)
              localStorage.setItem('gender_text', data.data.gender_text)
              localStorage.setItem('address', data.data.address)

              console.log(localStorage.getItem('email'))
              console.log(localStorage.getItem('name'))
              console.log(localStorage.getItem('phone'))
              console.log(localStorage.getItem('dob'))
              console.log(localStorage.getItem('gender'))
              console.log(localStorage.getItem('profile_picture'))
              console.log(localStorage.getItem('gender_text'))
              console.log(localStorage.getItem('address'))
              
            })
  }

}