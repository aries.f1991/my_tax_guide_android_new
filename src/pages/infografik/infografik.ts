import { AlertController, App, ModalController, NavController, ToastController, LoadingController } from 'ionic-angular'
import { Component} from '@angular/core'

import { LearningTaxDetailPage } from '../learning-tax-detail/learning-tax-detail'
import { SettingPage} from "../setting/setting"

import { InfografikData } from '../../providers/infografik-data'
import { UserData } from '../../providers/user-data'
import { HomePage } from '../home/home'

@Component({
  selector: 'page-infografik',
  templateUrl: 'infografik.html'
})
export class InfografikPage {

  public stateSearch = true;
  public commentImage = "assets/img/home/comment.png";

  listLearningTax: any = []
  totalLearningTax: any;
  positionPageLearningTax: any = 1;
  marginSearchShow: any = '0';

  constructor(
    public alertCtrl: AlertController,
    public app: App,

    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public toastCtrl: ToastController,

    public user: UserData,
    public infografikData: InfografikData
  ) {
    if(!localStorage.getItem('token'))
      this.user.valueToken()
    this.getDataInfografik(1)
  }

  ionViewDidLoad() {
    this.app.setTitle('Tax Rates')
  }

  ionViewWillEnter() {
  }

  onSearch(event){
    let message = event.target.value

    this.infografikData.getItemsSearch(message)
      .subscribe(
        (data: any) => {
          this.listLearningTax = []
          this.listLearningTax = data.data
        },
        (err) => {
          console.log(err)
        }
      )
  }

  doRefresh(){
  }
  
	doInfinite(infiniteScroll) {
    setTimeout(() => {
     this.doScroll();
      infiniteScroll.complete();
    }, 500);
  }
  
  doScroll(){
    let max = this.totalLearningTax / 10;
    if(this.positionPageLearningTax < max){
      this.positionPageLearningTax++;
      this.getDataInfografik(this.positionPageLearningTax)
    }
  }

  pushArray(a:any, b:any){
    for(let i = 0; i < b.length; i++){
      a.push(b[i])
    }
    return a
  }

  openSearch() {
    this.stateSearch = !this.stateSearch;
    if (!this.stateSearch){
      this.marginSearchShow = '55px'
    } else {
      this.marginSearchShow = '0'
    }
  }

  openSetting() {
    this.navCtrl.push(SettingPage)
  }

  goToDetail(idContent: any) {
    console.log(idContent)
    this.navCtrl.push(LearningTaxDetailPage, { idContent: idContent})
  }

  getDataInfografik(page){
    this.infografikData.getItems(page)
        .subscribe(
            (data: any) => {
              this.listLearningTax = this.pushArray(this.listLearningTax, data.data)
			  this.totalLearningTax = data.total_learning_tax
			  console.log(this.totalLearningTax)
            },
            (err) => {
              console.log(err)
			  this.presentToast()
            }
        )
  }
  
  openHome() {
    this.navCtrl.push(HomePage)
  }
  
  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Koneksi Gagal",
      duration: 3000,
      position: "bottom",
      showCloseButton: true,
      closeButtonText: 'Coba Lagi'
    });
    toast.present();

    toast.onDidDismiss(() => {
      this.openHome()
    });
  }
  
}
