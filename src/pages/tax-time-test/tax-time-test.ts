import { AlertController, App, ModalController, NavController, ToastController, LoadingController } from 'ionic-angular'
import { Component } from '@angular/core'

import { IndonesiaTaxGuideDetailPage } from '../indonesia-tax-guide-detail/indonesia-tax-guide-detail'
import { SettingPage} from "../setting/setting"
import { HomePage } from '../home/home'

import { TaxTimeTestData } from '../../providers/tax-time-test-data'
import { UserData } from '../../providers/user-data'

@Component({
  selector: 'page-time-test',
  templateUrl: 'tax-time-test.html'
})
export class TaxTimeTestPage {

  public stateSearch = true
  public commentImage = "assets/img/home/comment.png"
  public positionPage:any = 1
  listTaxGuide: any = []
  totalTaxGuide: any
  positionPageTaxGuide: any = 1
  marginSearchShow: any = '0';

  constructor(
    public alertCtrl: AlertController,
    public app: App,

    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public toastCtrl: ToastController,

    public user: UserData,
    public taxTimeTestData: TaxTimeTestData
  ) {
    if(!localStorage.getItem('token'))
			this.user.valueToken()
    this.getDataTaxTimeTest(1)
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter() {
  }

  onSearch(event){
    let message = event.target.value

    this.taxTimeTestData.getItemsSearch(message)
      .subscribe(
        (data: any) => {
          this.listTaxGuide = []
          this.listTaxGuide = data.data
		  
        },
        (err) => {
          console.log(err)
        }
      )
  }

  doRefresh(){
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
     this.doScroll();
      infiniteScroll.complete();
    }, 500);
  }
  
  doScroll(){
    let max = this.totalTaxGuide / 10
    if(this.positionPage < max){
      this.positionPage++
      this.getDataTaxTimeTest(this.positionPage)
    }
  }

  pushArray(a:any, b:any){
    for(let i = 0; i < b.length; i++){
      a.push(b[i])
    }
    return a
  }

  openSearch() {
    this.stateSearch = !this.stateSearch;
    if (!this.stateSearch){
      this.marginSearchShow = '55px'
    } else {
      this.marginSearchShow = '0'
    }
  }

  openSetting() {
    this.navCtrl.push(SettingPage)
  }

  goToDetail(idContent: any) {
    console.log(idContent)
    this.navCtrl.push(IndonesiaTaxGuideDetailPage, { idContent: idContent})
  }

  getDataTaxTimeTest(page){
    this.taxTimeTestData.getItems(page)
        .subscribe(
            (data: any) => {
				if (page === 1) {
                this.listTaxGuide = []
              }
              this.listTaxGuide = this.pushArray(this.listTaxGuide, data.data)
			  this.totalTaxGuide = data.total_indonesia_tax_guide
			  console.log(this.totalTaxGuide)
            },
            (err) => {
              console.log(err)
			  this.presentToast()
            }
        )
  }
  
  openHome() {
    this.navCtrl.push(HomePage)
  }
  
  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Koneksi Gagal",
      duration: 3000,
      position: "bottom",
      showCloseButton: true,
      closeButtonText: 'Coba Lagi'
    });
    toast.present();

    toast.onDidDismiss(() => {
      this.openHome()
    });
  }
  
}
