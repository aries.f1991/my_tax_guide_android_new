import { Component, ViewChild, ElementRef } from '@angular/core';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { AlertController } from 'ionic-angular';
import { ConferenceData } from '../../providers/conference-data';
import { ContactUsData } from '../../providers/contact-us-data'
import { NgForm } from '@angular/forms'

declare var google: any;

@Component({
  selector: 'contact-about',
  templateUrl: 'contact.html'
})
export class ContactPage {
  position = 1
  email
  company
  phone
  fullname
  message

  start:string

  @ViewChild('mapCanvas') mapElement: ElementRef;

  constructor(public confData: ConferenceData,
              public alertCtrl: AlertController,
              private launchNavigator: LaunchNavigator,
              public data: ContactUsData) { }
  
  removeForm(){
    this.message = ''
    this.company = ''
    this.phone = ''
    this.email = ''
    this.fullname = ''
  }            

  loadMap(){
    this.confData.getLocationOfficeMap().subscribe((mapData: any) => {
      let mapEle = this.mapElement.nativeElement;

      let infoWindow = new google.maps.InfoWindow({
        content: `<p><small>${mapData.find((d: any) => d.id == this.position).address}</small></p>`,
        maxWidth: 250
      });

      let map = new google.maps.Map(mapEle, {
        center: mapData.find((d: any) => d.id == this.position),
		fullscreenControl: false,
        zoom: 16
      });

      let marker = new google.maps.Marker({
          position: mapData.find((d: any) => d.id == this.position),
          map: map,
          title: mapData.find((d: any) => d.id == this.position).position
      });

      marker.addListener('click', () => {
        infoWindow.open(map, marker);
      });

      google.maps.event.addListenerOnce(map, 'idle', () => {
        mapEle.classList.add('show-map');
      });

    });
  }

  ionViewDidLoad() {
      this.loadMap()
  }

  selectPosition(pos){
    this.position = pos;
    this.loadMap()
  }

  callIT(mobNumber:string){
    window.open("tel:" + mobNumber);
  }

  onSubmit(f: NgForm){
    let sent = f.value

    if(!this.company)
      delete sent['company']
    if(!this.phone)
      delete sent['phone']
	if (sent.fullname === undefined)
		sent['name'] = ''
	if (sent.email === undefined)
		sent['email'] = ''
	if (sent.message === undefined)
		sent['message'] = ''
    this.sentMessage(sent)
  }

  sentMessage(sent) {
	var patt = new RegExp("[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})");
	if(patt.test(sent['email']))
	{
    this.data.setItem(0, sent, false)
      .subscribe(data => {
          this.presentAlert("Terima Kasih, Pesan anda berhasil terkirim!")
          this.removeForm()
          console.log(data)
      },
      err => {
		 var data = JSON.parse(err._body)
        this.presentAlert(data.message)
        console.log(err)
      })
	}
	else
	this.presentAlert("E-mail tidak sesuai")
  }

  goToLocation(){
    this.confData.getLocationOfficeMap().subscribe((mapData: any) => {
      let data = mapData.find((d: any) => d.id == this.position)
      let options: LaunchNavigatorOptions = {
        start: this.start
      };

      let destination
      destination = data.lat + ', ' + data.lng  
      this.launchNavigator.navigate(destination, options);
    })
  }

  presentAlert(message) {
    let alert = this.alertCtrl.create({
      subTitle: message,
      buttons: ['ok']
    });
    alert.present();
  }
}
