import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { NavController, LoadingController } from 'ionic-angular';
import { LoginRegisterData } from '../../providers/login-register-data'
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html'
})
export class ChangePasswordPage {

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public loginRegisterData: LoginRegisterData,
    public alertCtrl: AlertController,
  ) {
  }

  presentAlert(msg){
    let alert = this.alertCtrl.create({
      title: 'Info',
      message: msg,
      buttons: [
      {
        text: 'Ok',
        handler: () => {
          console.log('Button clicked');
        }
      }
      ]
    });
    alert.present();
  }

  onChangePassword(f: NgForm){
    let sent = f.value
    //cek datanya disini yaa
    console.log(sent)
    if(sent['password'] == sent['new_password'])
      this.editPassword(sent)
    else
      this.presentAlert("Password doesn't match")
  }

  editPassword(sent) {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    console.log(sent)
    // ganti end point buat edit profile
    this.loginRegisterData.setItem(0, sent, false, "-change_password")
      .subscribe(data => {
          loading.dismiss();
          console.log(data)
          this.presentAlert(data['message'])
          this.navCtrl.pop()
      },
      err => {
        this.presentAlert("Failed change password")
        console.log(err)
        loading.dismiss()
      })
  }

}
