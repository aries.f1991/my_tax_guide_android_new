import { Component } from '@angular/core'
import { NavController, NavParams,ToastController, Platform } from 'ionic-angular'
import { SocialSharing } from '@ionic-native/social-sharing'
import { SettingPage } from "../setting/setting"
import { VideoData } from '../../providers/video-data'
import { AlertController } from 'ionic-angular'
import { DomSanitizer } from '@angular/platform-browser';
import { HomePage } from '../home/home'

@Component({
  selector: 'page-learning-tax-detail',
  templateUrl: 'learning-tax-detail.html'
})

export class LearningTaxDetailPage {
  fontSize: any = localStorage.getItem('text_size') + 'px'
  id: any
  session: any
  detailData: any
  dataImageContent: any
  dataRelatedContent: any
  dataComment: any
  photoStatus: boolean = false
  shareUrlStatus: boolean = false;
  lengthArrImg: boolean = false;

  width: any;
  height: any;
  pinchW: any;
  pinchH: any;
  timeout: any;

  name: any
  comment: any
  commentStatus: boolean = false
  commentDisable: boolean = false
  contributorType: any = 3

  positionComment: any = 1
  commentStop: boolean = true

  constructor(
    public learningTaxData: VideoData,
    public navParams: NavParams,
    public alertCtrl: AlertController,
	public toastCtrl: ToastController,
    public socialSharing: SocialSharing,
	public platform: Platform,
    public navCtrl: NavController,
    private sanitizer: DomSanitizer
  ) {

    if(localStorage.getItem("login_token")){
      this.name = localStorage.getItem("name")
      this.commentDisable = true
      this.contributorType = 2
    }
    this.id = this.navParams.data.idContent
    this.getLearningTaxDetailData()
    this.getLearningTaxDetailDataImage()
    this.getLearningTaxDetailDataComment(1)
    this.getLearningTaxDetailDataRelated()
  }
	
  share(subject, url)
  {
    this.socialSharing.share(null, subject, null, url)
      .then(() =>{
        console.log("success sharing url")
      }).catch((err) => {
      console.log(err)
    })
  }

  ionViewWillEnter() {
  }

  getLearningTaxDetailData(){
    this.learningTaxData.getItem(this.id)
      .subscribe(
        (data: any) => {
          if(data){
            this.detailData = data
            if(this.detailData.data.url_share != '') {
              this.shareUrlStatus = true
            }
            this.detailData.data.content = this.toHTML(this.detailData.data.content);
          }
        },
        (err) => {
          console.log(err)
		  this.presentToast()
        }
    )
  }

  getLearningTaxDetailDataImage(){
    this.learningTaxData.getItemImage(this.id)
      .subscribe(
        (data: any) => {
          if(data){
            this.dataImageContent = data.data;
            if (data.data.length == 1){
              this.lengthArrImg = true
            }
            this.photoStatus = true
          }
        },
        (err) => {
          console.log(err)
        }
    )
  }
  
  openHome() {
    this.navCtrl.push(HomePage)
  }
  
  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Koneksi Gagal",
      duration: 3000,
      position: "bottom",
      showCloseButton: true,
      closeButtonText: 'Coba Lagi'
    });
    toast.present();

    toast.onDidDismiss(() => {
      this.openHome()
    });
  }

  getLearningTaxDetailDataComment(page){
    this.learningTaxData.getItemComment(this.id,page)
      .subscribe(
        (data: any) => {
          if(data){
            if (page === 1) {
              this.dataComment = []
            }
            if(data.data.length === 0){
              this.commentStatus = false
            }
            this.dataComment = this.pushArray(this.dataComment, data.data)
          }
        },
        (err) => {
          console.log(err)
        }
    )
  }

  getLearningTaxDetailDataRelated(){
    this.learningTaxData.getItemRelated(this.id)
      .subscribe(
        (data: any) => {
          if(data)
            this.dataRelatedContent = data.data
        },
        (err) => {
          console.log(err)
        }
    )
  }

   postComment(creds){
    this.learningTaxData.setItem(this.id, creds, false, "comment")
      .subscribe(data => {
          this.comment = ''
          this.commentStatus = false
          this.presentAlert("Terima Kasih, Komentar anda berhasil terkirim!")
          console.log(data)
      },
      err => {
        this.presentAlert(err)
        console.log(err)
      })
  }

  presentAlert(message) {
    let alert = this.alertCtrl.create({
      subTitle: message,
      buttons: ['ok']
    })
    alert.present();
  }

  sendComment(){
    if(this.name !== undefined && this.comment !== undefined){
      let comment
      comment = {}

      comment.type = 3
      comment.text_id = this.id
      comment.contributor_type = this.contributorType
      comment.contributor_id = localStorage.getItem("id")
      comment.alias = this.name
      comment.content = this.comment

      this.postComment(comment)
      console.log(comment)
    }else{
      this.presentAlert("Mohon lengkapi kolom nama dan komentar!")
    }

  }

  openComment(){
    this.commentStatus = true
  }

  closeComment(){
    this.commentStatus = false
  }

  openSetting() {
    this.navCtrl.push(SettingPage)
  }

  goToRelated(idContent: any) {
    console.log(idContent);
    this.navCtrl.push(LearningTaxDetailPage, { idContent: idContent})
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
     this.doScroll();
      infiniteScroll.complete();
    }, 500);
  }

  doScroll(){
    if(this.commentStop){
      this.positionComment++
      this.getLearningTaxDetailDataComment(this.positionComment)
    }
  }

  pushArray(a:any, b:any){
    for(let i = 0; i < b.length; i++){
      a.push(b[i])
    }
    return a
  }

  toHTML(input) : any {
    let result = input.replace(/&amp;/g, "&").replace(/&quot;/g, '"').replace(/&lt;/g, "<").replace(/&gt;/g, ">");
    // result =  new DOMParser().parseFromString(result, "text/html").documentElement.textContent;
    result = this.sanitizer.bypassSecurityTrustHtml(result);
    return result;
  }

  pinchEvent(e) {
    console.log(e);
    this.width = this.pinchW * e.scale;
    this.height = this.pinchH * e.scale;

    if(this.timeout == null){
      this.timeout = setTimeout(() => {
        this.timeout = null;
        this.updateWidthHeightPinch();
      }, 1000);
    } else {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        this.timeout = null;
        this.updateWidthHeightPinch();
      }, 1000);
    }
  }

  updateWidthHeightPinch() {
    this.pinchW = this.width;
    this.pinchH = this.height;
  }
}

