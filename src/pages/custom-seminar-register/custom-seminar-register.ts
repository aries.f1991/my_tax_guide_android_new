import { Component } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import { SettingPage} from "../setting/setting";
import { SeminarData } from '../../providers/seminar-data';
import { NgForm } from '@angular/forms'
import { AlertController } from 'ionic-angular';
import { CustomSeminarPage } from '../custom-seminar/custom-seminar'

@Component({
  selector: 'page-custom-seminar-register',
  templateUrl: 'custom-seminar-register.html'
})
export class CustomSeminarRegisterPage {
  id: any
  userCount: any
  users: any = []
  listSeminarRelated: any = []
  listSeminarSelected: any = []
  fullnameRegisterPeople: any = []
  companyRegisterPeople: any = []
  emailRegisterPeople: any = []

  startDate: any = []
  endDate: any = []

  constructor(
    public seminarData: SeminarData,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public navCtrl: NavController
  ) {
    this.id = this.navParams.data.idContent;
    this.userCount = this.navParams.data.user;
    this.startDate = this.navParams.data.startdate;
    this.endDate = this.navParams.data.enddate;

    this.listSeminarSelected[0] = +this.id

    for(let i = 0; i < this.userCount; i++){
      this.users[i] = {}
    }

    this.getDataSeminarRelated(this.startDate, this.endDate)
  }

  onSubmit(f: NgForm){
    let data = f.value

    let sent = []
	var patt = new RegExp("[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})");
	
    for(let i = 0; i < this.listSeminarSelected.length; i++){
      sent['seminar_id[' + i + ']'] = this.listSeminarSelected[i]
    }
	if(data.fullname === undefined)
		sent['nama_pendaftar'] = ''
		else
		sent['nama_pendaftar'] = data.fullname
	if(data.email === undefined || !patt.test(data.email))
		sent['email_pendaftar'] = ''
		else
    sent['email_pendaftar'] = data.email
    sent['company_pendaftar'] = data.company
    sent['phone_pendaftar'] = data.phone

    for(let i = 0; i < this.userCount; i++){
	if(this.fullnameRegisterPeople[i] === undefined)
		sent['nama_peserta[' + i + ']'] = ''
		else
      sent['nama_peserta[' + i + ']'] = this.fullnameRegisterPeople[i]
	if(this.emailRegisterPeople[i] === undefined || !patt.test(this.emailRegisterPeople[i]))
		sent['email_peserta[' + i + ']'] = ''
	else
      sent['email_peserta[' + i + ']'] = this.emailRegisterPeople[i]
      sent['company_peserta[' + i + ']'] = this.companyRegisterPeople[i]
    }

    this.sentRegister(sent)
  }

  sentRegister(sent) {
    this.seminarData.setItem(0, sent, false, '-registration')
      .subscribe(data => {
          this.presentAlert("Terima kasih, Pendaftaran anda telah dikirim silahkan cek email anda!")
          this.removeForm()
          this.navCtrl.push(CustomSeminarPage)
          console.log(data)
        },
        err => {
		var data = JSON.parse(err._body)
          this.presentAlert("Gagal! " + data.message)
          console.log(err)
        })
  }

  openSetting() {
    this.navCtrl.push(SettingPage)
  }

  presentAlert(message) {
    let alert = this.alertCtrl.create({
      subTitle: message,
      buttons: ['ok']
    });
    alert.present();
  }

  removeForm(){
    this.emailRegisterPeople = []
    this.fullnameRegisterPeople = []
    this.companyRegisterPeople = []
  }

  getDataSeminarRelated(startdate, enddate) {
    const param = '?date_start=' + startdate + '&date_end=' + enddate
    console.log(param)
    this.seminarData.getItems("", "-day" , param)
      .subscribe(
        (data: any) => {
          this.listSeminarRelated = []
          this.listSeminarRelated = data.data
        },
        (err) => {
          console.log(err)
        }
      )
  }

  updateSelected(id){
    console.log(id)
    const index =  this.listSeminarSelected.findIndex(number => number == id)

    if(index == -1){
      this.listSeminarSelected.push(+id)
    } else {
      this.listSeminarSelected.splice(index, 1)
    }
    console.log(this.listSeminarSelected)
  }

  convertDateText(date){
    var inputDate = new Date(date)
    var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ]

    var day = inputDate.getDate()
    var monthIndex = inputDate.getMonth()
    var year = inputDate.getFullYear()

    return day + ' ' + monthNames[monthIndex] + ' ' + year
  }
}

