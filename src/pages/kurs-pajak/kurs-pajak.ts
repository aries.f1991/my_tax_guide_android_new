import { AlertController, App, ModalController, NavController, ToastController, LoadingController } from 'ionic-angular'
import { Component } from '@angular/core'

import { IndonesiaTaxGuideDetailPage } from '../indonesia-tax-guide-detail/indonesia-tax-guide-detail'
import { SettingPage} from "../setting/setting"
import { HomePage } from '../home/home'

import { KursPajakData } from '../../providers/kurs-pajak-data'
import { UserData } from '../../providers/user-data'

@Component({
  selector: 'page-kurs-pajak',
  templateUrl: 'kurs-pajak.html'
})
export class KursPajakPage {

  public stateSearch = true;
  public commentImage = "assets/img/home/comment.png";

  listTaxGuide: any = []
  totalTaxGuide: any;
  positionPageTaxGuide: any = 1;
  marginSearchShow: any = '0';

  constructor(
    public alertCtrl: AlertController,
    public app: App,

    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public toastCtrl: ToastController,

    public user: UserData,
    public kursPajakData: KursPajakData
  ) {
    if(!localStorage.getItem('token'))
      this.user.valueToken()
    this.getDataKursPajak(1)
  }

  ionViewDidLoad() {
    this.app.setTitle('Tax Rates')
  }

  ionViewWillEnter() {
  }

  onSearch(event){
    let message = event.target.value

    this.kursPajakData.getItemsSearch(message)
      .subscribe(
        (data: any) => {
          this.listTaxGuide = []
          this.listTaxGuide = data.data
        },
        (err) => {
          console.log(err)
        }
      )
  }

  doRefresh(){
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
     this.doScroll();
      infiniteScroll.complete();
    }, 500);
  }
  
  doScroll(){
    let max = this.totalTaxGuide / 10;
    if(this.positionPageTaxGuide < max){
      this.positionPageTaxGuide++;
      this.getDataKursPajak(this.positionPageTaxGuide)
    }
  }

  pushArray(a:any, b:any){
    for(let i = 0; i < b.length; i++){
      a.push(b[i])
    }
    return a
  }

  openSearch() {
    this.stateSearch = !this.stateSearch;
    if (!this.stateSearch){
      this.marginSearchShow = '55px'
    } else {
      this.marginSearchShow = '0'
    }
  }

  openSetting() {
    this.navCtrl.push(SettingPage)
  }

  goToDetail(idContent: any) {
    console.log(idContent);
    this.navCtrl.push(IndonesiaTaxGuideDetailPage, { idContent: idContent})
  }

  getDataKursPajak(page){
    this.kursPajakData.getItems(page)
        .subscribe(
            (data: any) => {
              this.listTaxGuide = this.pushArray(this.listTaxGuide, data.data)
			  this.totalTaxGuide = data.total_indonesia_tax_guide
			  console.log(this.totalTaxGuide)
            },
            (err) => {
              console.log(err)
			  this.presentToast()
            }
        )
  }
  
  openHome() {
    this.navCtrl.push(HomePage)
  }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Koneksi Gagal",
      duration: 3000,
      position: "bottom",
      showCloseButton: true,
      closeButtonText: 'Coba Lagi'
    });
    toast.present();

    toast.onDidDismiss(() => {
      this.openHome()
    });
  }
}
