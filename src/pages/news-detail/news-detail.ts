import { Component } from '@angular/core';
import { NavParams, NavController, ToastController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { NewsData } from '../../providers/news-data';
import { AlertController } from 'ionic-angular';
import { HomePage } from '../home/home'

@Component({
  selector: 'page-session-detail',
  templateUrl: 'news-detail.html'
})
export class NewsDetailPage {
  fontSize: any = localStorage.getItem('text_size') + 'px'
  id: any
  session: any
  detailData: any
  dataImageContent: any
  dataRelatedContent: any
  dataComment: any
  photoStatus: boolean = false
  shareUrlStatus: boolean = false
  lengthArrImg: boolean = false;

  name: any
  comment: any
  commentStatus: boolean = false
  commentDisable: boolean = false
  contributorType: any = 3

  positionComment: any = 1
  commentStop: boolean = true

  public commentImage = "assets/img/home/comment.png"

  constructor(
    public newsData: NewsData,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public navCtrl: NavController,
	public toastCtrl: ToastController,
    public socialSharing: SocialSharing,
  ) {
    if(localStorage.getItem("login_token")){
      this.name = localStorage.getItem("name")
      this.commentDisable = true
      this.contributorType = 2
    }

    this.id = this.navParams.data.idContent
    this.getNewsData()
    this.getNewsDataImage()
    this.getNewsDataComment(1)
    this.getNewsDataRelated()
  }

  share(subject, url)
  {
    this.socialSharing.share(null, subject, null, url)
      .then(() =>{
        console.log("success sharing url")
      }).catch((err) => {
        console.log(err)
      });
  }

  ionViewWillEnter() {
  }

  getNewsData(){
    this.newsData.getItem(this.id)
      .subscribe(
        (data: any) => {
          if(data){
            this.detailData = data
            if(this.detailData.data.url_share != '') {
              this.shareUrlStatus = true
            }
          }
        },
        (err) => {
          console.log(err)
		  this.presentToast()
        }
    )
  }
	
  openHome() {
    this.navCtrl.push(HomePage)
  }
  
  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Koneksi Gagal",
      duration: 3000,
      position: "bottom",
      showCloseButton: true,
      closeButtonText: 'Coba Lagi'
    });
    toast.present();

    toast.onDidDismiss(() => {
      this.openHome()
    });
  }
  
  getNewsDataImage(){
    this.newsData.getItemImage(this.id)
      .subscribe(
        (data: any) => {
          if(data) {
            this.dataImageContent = data.data;
            if (data.data.length == 1){
              this.lengthArrImg = true
            }
            this.photoStatus = true
          }
          console.log(data)
        },
        (err) => {
          console.log(err)
        }
    )
  }

  getNewsDataComment(page){
    this.newsData.getItemComment(this.id,page)
      .subscribe(
        (data: any) => {
          if(data){
            if (page === 1) {
              this.dataComment = []
            }
            if(data.data.length === 0){
              this.commentStatus = false
            }
            this.dataComment = this.pushArray(this.dataComment, data.data)
          }
        },
        (err) => {
          console.log(err)
        }
    )
  }

  getNewsDataRelated(){
    this.newsData.getItemRelated(this.id)
      .subscribe(
        (data: any) => {
          if(data)
            this.dataRelatedContent = data.data
          console.log(data)
        },
        (err) => {
          console.log(err)
        }
    )
  }

  postComment(creds){
    this.newsData.setItem(this.id, creds, false, "comment")
      .subscribe(data => {
          this.comment = ''
          this.commentStatus = false
          this.presentAlert("Terima Kasih, Komentar anda berhasil terkirim!")
          console.log(data)
      },
      err => {
        this.presentAlert(err)
        console.log(err)
      })
  }

  presentAlert(message) {
    let alert = this.alertCtrl.create({
      subTitle: message,
      buttons: ['ok']
    })
    alert.present();
  }

  sendComment(){
    if(this.name !== undefined && this.comment !== undefined){
      let comment
      comment = {}

      comment.type = 1
      comment.text_id = this.id
      comment.contributor_type = this.contributorType
      comment.contributor_id = localStorage.getItem("id")
      comment.alias = this.name
      comment.content = this.comment

      this.postComment(comment)
      console.log(comment)
    }else{
      this.presentAlert("Mohon lengkapi kolom nama dan komentar!")
    }

  }

  openComment(){
    this.commentStatus = true
  }

  closeComment(){
    this.commentStatus = false
  }

  goToRelated(idContent: any) {
    console.log(idContent);
    this.navCtrl.push(NewsDetailPage, { idContent: idContent})
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
     this.doScroll();
      infiniteScroll.complete();
    }, 500);
  }

  doScroll(){
    if(this.commentStop){
      this.positionComment++
      this.getNewsDataComment(this.positionComment)
    }
  }

  pushArray(a:any, b:any){
    for(let i = 0; i < b.length; i++){
      a.push(b[i])
    }
    return a
  }

  toHTML(input) : any {
    return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
  }
}

