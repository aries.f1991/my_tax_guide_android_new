import { ViewController, NavParams, AlertController, App, ModalController, NavController, ToastController, LoadingController, Platform } from 'ionic-angular'
import { Component } from '@angular/core'

import { CustomSeminarDetailPage } from '../custom-seminar-detail/custom-seminar-detail'
import { SettingPage } from "../setting/setting"

import { SeminarData } from '../../providers/seminar-data'
import { UserData } from '../../providers/user-data'
import { CalendarModalOptions, DayConfig } from "ion2-calendar";

@Component({
  selector: 'page-custom-seminar',
  templateUrl: 'custom-seminar.html'
})
export class CustomSeminarPage {
  date: string
  public putarArah = "assets/img/putar.png"
  type: 'string'
  eventSource = []
  viewTitle: string
  selectedDay = new Date()
  calendarOptions: CalendarModalOptions

  calendar = {
    mode: 'month',
    currentDate: new Date()
  };

  stateSearch = true
  segment = 'calendar'
  groups: any = []
  confDate: string
  
  listSeminar: any = []
  listSeminarMonth: any = []
  totalSeminar: any
  positionPageSeminar: any = 1

  constructor(
    public alertCtrl: AlertController,
    public app: App,

    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public toastCtrl: ToastController,
	public platform: Platform,

    public user: UserData,
    public seminarData: SeminarData,
  ) {
    if(!localStorage.getItem('token'))
      this.user.valueToken()

    var todayDate = new Date();

    let month = todayDate.getMonth()
    let year = todayDate.getFullYear()

    this.getDataSeminar(1)
    this.getDataSeminarMonth(month + 1, year)
  }

  onChange($event) {
    console.log($event);
    let detailCalendarDayModal = this.modalCtrl.create(CustomSeminarModal, { date: $event._d });
    detailCalendarDayModal.present();
  }

  configureCalendar() {
    let _daysConfig: DayConfig[] = [];

    for (let i = 0; i < this.listSeminarMonth.length; i++) {
      _daysConfig.push({
        date: new Date(this.listSeminarMonth[i].seminar_start),
        marked: true,
        subTitle: `seminar`
      })
    }

    this.calendarOptions = {
      daysConfig: _daysConfig
    };
  }

  monthChange($event) {
    this.getDataSeminarMonth($event.newMonth.months, $event.newMonth.years)
  }

  addEvent() {
    let modal = this.modalCtrl.create('EventModalPage', {selectedDay: this.selectedDay});
    modal.present();
    modal.onDidDismiss(data => {
      if (data) {
        let eventData = data;

        eventData.startTime = new Date(data.startTime);
        eventData.endTime = new Date(data.endTime);

        let events = this.eventSource;
        events.push(eventData);
        this.eventSource = [];
        setTimeout(() => {
          this.eventSource = events;
        });
      }
    });
  }

  onViewTitleChanged(title) {
    this.viewTitle = title;
  }

  onEventSelected(event) {
    let start = (event.startTime).format('LLLL');
    let end = (event.endTime).format('LLLL');

    let alert = this.alertCtrl.create({
      title: '' + event.title,
      subTitle: 'From: ' + start + '<br>To: ' + end,
      buttons: ['OK']
    })
    alert.present();
  }

  onTimeSelected(ev) {
    this.selectedDay = ev.selectedTime;
  }
  
  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Koneksi Gagal",
      duration: 3000,
      position: "bottom",
      showCloseButton: true,
      closeButtonText: 'Coba Lagi'
    });
    toast.present();

    toast.onDidDismiss(() => {
      this.doRefresh()
    });
  }
  
  getDataSeminar(page) {
    this.seminarData.getItems(page)
        .subscribe(
            (data: any) => {
              this.listSeminar = this.pushArray(this.listSeminar, data.data)
              this.totalSeminar = data.total_Seminar
            },
            (err) => {
              console.log(err)
			  this.presentToast()
            }
        )
  }

  getDataSeminarMonth(month, year) {
    const param = '?month=' + month + '&year=' + year
    console.log(param)
    this.seminarData.getItems("", "-month" , param)
        .subscribe(
            (data: any) => {
              this.listSeminarMonth = []
              this.listSeminarMonth = data.data
              this.configureCalendar()
            },
            (err) => {
              console.log(err)
			  this.presentToast()
            }
        )
  }

  onSearch(event){
    let message = event.target.value
    this.seminarData.getItemsSearch(message)
      .subscribe(
          (data: any) => {
            this.listSeminar = data.data
          },
          (err) => {
            console.log(err)
          }
      )    
  }

  doRefresh(){
    this.listSeminar = []
    this.positionPageSeminar = 1
    this.getDataSeminar(1)
  }

  doScroll(){
    let max = Math.floor(this.totalSeminar / 5)
    if(this.positionPageSeminar < max){
      this.positionPageSeminar++
      this.getDataSeminar(this.positionPageSeminar)
    }
  }

  pushArray(a:any, b:any){
    for(let i = 0; i < b.length; i++){
      a.push(b[i])
    }
    return a
  }

  openSearch() {
    this.stateSearch = !this.stateSearch
  }

  openSetting() {
    this.navCtrl.push(SettingPage)
  }

  goToDetail(idContent: any) {
    this.navCtrl.push(CustomSeminarDetailPage, { idContent: idContent })
  }

  statusDateConvert(dateClose){
    var inputDate = new Date(dateClose);
    var todayDate = new Date();

    if(inputDate.setHours(0,0,0,0) < todayDate.setHours(0,0,0,0)) {
      return "Close"
    }else{
      return "Open"
    }
  }

  convertDateText(date){
    var inputDate = new Date(date)
    var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ]
  
    var day = inputDate.getDate()
    var monthIndex = inputDate.getMonth()
    var year = inputDate.getFullYear()
  
    return day + ' ' + monthNames[monthIndex] + ' ' + year
  }
}

// modal controller

@Component({
  selector: 'modal-custom-seminar',
  templateUrl: 'custom-seminar-modal.html'
})
export class CustomSeminarModal {
  date: string
  dateFormat: any
  listSeminar: any = []
  public isEmpty: any = false

  constructor(
    public app: App,
    public navCtrl: NavController,
    public user: UserData,
    public seminarData: SeminarData,
    public viewCtrl: ViewController,
    public params: NavParams
  ) {
    if(!localStorage.getItem('token'))
      this.user.valueToken()

    this.date = params.get('date')
    this.dateFormat = this.formatDate(this.date)

    console.log('date', params.get('date'));
    console.log('date', this.dateFormat);

    this.getDataSeminarDay(this.dateFormat, this.dateFormat)
  }

  getDataSeminarDay(startdate, enddate) {
    const param = '?limit=5&date_start=' + startdate + '&date_end=' + enddate
    console.log(param)
    this.seminarData.getItems("", "-daySeminar" , param)
        .subscribe(
            (data: any) => {
				if(data.data.length === 0){
				this.isEmpty = true}
				else{
					this.isEmpty = false;
              this.listSeminar = []
			this.listSeminar = data.data}
            },
            (err) => {
              console.log(err)
			  
            }
        )
  }

  goToDetail(idContent: any) {
    this.navCtrl.push(CustomSeminarDetailPage, { idContent: idContent })
  }

  statusDateConvert(dateClose){
    var inputDate = new Date(dateClose);
    var todayDate = new Date();

    if(inputDate.setHours(0,0,0,0) < todayDate.setHours(0,0,0,0)) {
      return "Close"
    }else{
      return "Open"
    }
  }

  convertDateText(date){
    var inputDate = new Date(date)
    var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ]
  
    var day = inputDate.getDate()
    var monthIndex = inputDate.getMonth()
    var year = inputDate.getFullYear()
  
    return day + ' ' + monthNames[monthIndex] + ' ' + year
  }

  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }
  

}
