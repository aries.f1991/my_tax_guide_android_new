import { AlertController, App, ModalController, NavController, ToastController, LoadingController, Slides, Platform} from 'ionic-angular'
import { Component, ViewChild } from '@angular/core'
import { NgForm } from '@angular/forms';
import { TaxAndCustomRegulationDetailPage } from '../tax-and-custom-regulation-detail/tax-and-custom-regulation-detail'
// import { NewsDetailPage } from '../news-detail/news-detail'
import { LoginPage} from "../login/login"
import { SettingPage} from "../setting/setting"

import { ArticleData } from '../../providers/article-data'
import { NewsData } from '../../providers/news-data'
import { UserData } from '../../providers/user-data'
import { SlideData } from '../../providers/slider-data'
import { TaxCustomRegulationData } from '../../providers/tax-custom-regulation-data'

@Component({
  selector: 'page-tax-and-custom-regulation',
  templateUrl: 'tax-and-custom-regulation.html'
})
export class TaxAndCustomRegulationPage {

  public stateSearch = true
  public isStart = 1
  public newsImage = "assets/img/home/newspaper-icon-9.png"
  public articleImage = "assets/img/home/article.png"
  public commentImage = "assets/img/home/comment.png"
  public notFound = "assets/img/notFound.png"
  public putarArah = "assets/img/putar.png"
  public notAccess = "assets/img/akses.png"
  public topik: any = []
  public level: any = []
  public regulationItems: any
  public isEmptyData: any = false
  public totalTaxCustomRegulation: any
  public parameter: any
  public positionPage:any = 1

  marginSearchShow: any = '10px';

  @ViewChild('slides') slides: Slides

  segment = 'news'
  groups: any = []
  confDate: string

  dataSlide: any
  listSlide: any = [
    {
      "id": "0",
      "type_id": "0",
      "title": "Default Slide",
      "date_created": "Today",
      "primary_image": "assets/img/placeholder-image.png"
    }
    ];

  listArticle: any =[]
  totalArticle: any
  positionPageArticle: any = 1

  listNews: any = []
  totalNews: any
  positionPageNews: any = 1

  constructor(
    public alertCtrl: AlertController,
    public app: App,
    public taxRegulationData: TaxCustomRegulationData,
    // public taxCustomRegulationDetailPage: TaxAndCustomRegulationDetailPage,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
	public platform: Platform,

    public user: UserData,
    public newsData: NewsData,
    public sliderData: SlideData,
    public articleData: ArticleData
  ) {
  }

  ionViewDidLoad() {
    // this.slides.update()
    this.app.setTitle('Tax Rates')
	this.isStart = 1
	this.stateSearch = true
  }
  
   openSearch() {
    this.stateSearch = !this.stateSearch;
	this.isStart = 0;
    if (!this.stateSearch){
      this.marginSearchShow = '1000px'
    } else {
      this.marginSearchShow = '10px'
    }
  }
  
  openSetting() {
    this.navCtrl.push(LoginPage)
  }
  
  openTrueSetting() {
    this.navCtrl.push(SettingPage)
  }

  ionViewWillEnter() {
    this.getListLevel()
    this.getListTopik()
	this.isStart = 1
	this.stateSearch = true
  }

  getListTopik()
  {
    let loading = this.loadingCtrl.create({
    content: 'Mohon tunggu...'
  });
  loading.present();
    this.taxRegulationData.nameClass = 'kurs-pajak-topik'
    this.taxRegulationData.getItems("")
      .subscribe(
            (data: any) => {
              // console.log(data)
              loading.dismiss()
              let kosong = {}
              kosong['id'] = undefined
              kosong['name'] = "Null"

              this.topik = data.data
              this.topik = this.pushArray(this.topik, kosong)
              console.log("topik " + this.topik)
              console.log(data)

            },
            (err) => {
              loading.dismiss()
              console.log(err)
            }
        )
  }

  getListLevel()
  {
    let loading = this.loadingCtrl.create({
    content: 'Mohon tunggu...'
  });
  loading.present();
    this.taxRegulationData.nameClass = 'kurs-pajak-level'
    this.taxRegulationData.getItems("")
      .subscribe(
            (data: any) => {

              loading.dismiss()
              let kosong = {}
              kosong['id'] = undefined
              kosong['name'] = "Null"
              this.level = data.data
              this.level = this.pushArray(this.level, kosong)
              console.log("level" + this.level)
              console.log(data)
            },
            (err) => {
              loading.dismiss()
              console.log(err)
            }
        )
  }

  search(f: NgForm)
  {
    if(localStorage.getItem('login_token'))
    {
      this.parameter = f.value
	  this.stateSearch = true
      console.log(this.parameter)
      this.getDataTaxRegulation(this.parameter, 1)
    }
    else
    {
      this.presentToast()
    }
  }

  getDataTaxRegulation(sent, page)
  {
     let loading = this.loadingCtrl.create({
    content: 'Mohon tunggu...'
	});
    this.taxRegulationData.nameClass = 'kurs-pajak-searchTaxRegulation'
    this.taxRegulationData.getItemsSearchTaxRegulation(sent, page)
      .subscribe(data => {
        if(page == 1)
        {
          this.regulationItems = []
        }
        if(data.data.length === 0){
          this.isEmptyData = true
        } else {
          this.isEmptyData = false;
          this.regulationItems = this.pushArray(this.regulationItems, data.data)
          this.totalTaxCustomRegulation = data.total_tax_custom_regulation
        }
        console.log(this.regulationItems)
		loading.dismiss()
      },
      (err) =>{
        console.log(err)
		loading.dismiss()
      }
      )
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
     this.doScroll();
      infiniteScroll.complete();
    }, 500);
  }

  doScroll(){
    console.log(this.segment)

    let max = this.totalTaxCustomRegulation / 10
    if(this.positionPage < max){
      this.positionPage++
      this.getDataTaxRegulation(this.parameter, this.positionPage)
    }
  }

  pushArray(a:any, b:any){
    for(let i = 0; i < b.length; i++){
      a.push(b[i])
    }
    return a
  }

  goToDetail(id : any) {
    this.navCtrl.push(TaxAndCustomRegulationDetailPage, {id: id})
  }
  
  checkLogin() {
	if(localStorage.getItem('login_token'))
	  return 1
	else
	  return 0
  }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Login first to search tax regulation",
      duration: 3000,
      position: "bottom",
      showCloseButton: true,
      closeButtonText: 'Login'
    });
    toast.present();

    toast.onDidDismiss(() => {
      this.openSetting()
    });
  }

  }
